package fit;

import java.util.regex.Pattern;

public abstract class MapBinding extends Binding {
    private static final Pattern regexMethodPattern = Pattern.compile("(.+)(?:\\?\\?|!!)");
    private static final Pattern methodPattern = Pattern.compile("(.+)(?:\\(\\)|\\?|!)");
    private static final Pattern fieldPattern = Pattern.compile("=?([^=]+)=?");

    public static Binding create(Fixture fixture, String name) throws Throwable {
        Binding binding = null;
        String columnName = name;

        if (name.startsWith("=")) {
            binding = new SaveBinding();
            columnName = name.substring(1);
        } else if (name.endsWith("=")) {
            binding = new RecallBinding();
            columnName = name.substring(0, name.length() - 1);
        } else if (regexMethodPattern.matcher(name).matches())
            binding = new RegexQueryBinding();
        else if (methodPattern.matcher(name).matches())
            binding = new QueryBinding();
        else if (fieldPattern.matcher(name).matches())
            binding = new SetBinding();

        if (binding == null)
            binding = new NullBinding();
        else {
            binding.adapter = new MapTypeAdapter(fixture, columnName);
        }

        return binding;
    }
}

