package fit;

import fit.exception.FitFailureException;

import java.lang.reflect.Field;
import java.text.Format;
import java.util.HashMap;
import java.util.Map;

/**
 * The MapTypeAdapter understands Maps, Objects and Strings. Any object pushed into the adapter
 * is converted to a String on the way out. Any method receiving an object converts it to a
 * String before doing anything useful with it. The ensures that like is always compared with
 * like.
 * <p/>
 * When working with a Map implementation, it will only attempt to extract the value from the
 * map that corresponds to the key provided to the adapter at instantiation time.
 * <p/>
 * When dealing with an object, it will look through a registered set of Formatters to convert
 * the object to a String. If none is found it will simply call the toString() method.
 * <p/>
 * Within the translation this object performs, null values are preserved. That is, if you pass
 * null in expect to receive null back!
 */
public class MapTypeAdapter extends TypeAdapter {
    Map<Class<?>, Format> formats;
    boolean recall = false;
    String key;

    public MapTypeAdapter(Fixture fixture, String cellName) {
        //these attributes are required for Fitnesse behaviour to work as expected.
        Field formatField;
        Map<Class<?>, Format> formatters = null;
        try {
            field = fixture.getClass().getField("map");
            target = field.get(fixture);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("The map field doesn't exist on the MapFixture", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("The map field doesn't exist on the MapFixture", e);
        }
        try {
            formatField = fixture.getClass().getField("formats");
            formatters = (Map<Class<?>, Format>) formatField.get(fixture);
        } catch (Exception e) {
            //ignore
        }
        configure(formatters, cellName);
    }

    public MapTypeAdapter(Map<Class<?>, Format> formatters, String cellName) {
        configure(formatters, cellName);
    }

    private void configure(Map<Class<?>, Format> formatters, String cellName) {
        formats = formatters;
        if (formats == null) {
            formats = new HashMap<Class<?>, Format>();
        }

        type = String.class;
        key = cellName;
        if (cellName.endsWith("=")) {
            recall = true;
            key = cellName.substring(0, cellName.length() - 1);
        }
        int index = cellName.indexOf(":");
        if (index != -1) {
            key = cellName.substring(0, index);
            String className = cellName.substring(index + 1);
            for (Class clazz : formats.keySet()) {
                String clazzName = clazz.getName();
                if (clazzName.substring(clazzName.lastIndexOf(".") + 1).equals(className)) {
                    type = clazz;
                    break;
                }
            }
        }
    }

    @Override
    public Object get() {
        Object o = ((Map) target).get(key);
        if (o == null) return null;
        Format format = formats.get(type);
        return format == null ? o.toString() : format.format(o);
    }

    /**
     * No methods on the underlying map will be invoked. This method will simply do
     * a get().
     *
     * @return The string representation of the underlying value.
     */
    @Override
    public Object invoke() {
        return get();
    }

    @Override
    public void set(Object value) throws Exception {
        ((Map<Object, Object>) target).put(key, value);
    }

    @Override
    public Object parse(String s) throws Exception {
        Format format = formats.get(type);
        if (recall && Fixture.getSymbol(s) == null) {
            throw new FitFailureException("No such symbol: "+s);
        }
        String realString = (recall ? (String)Fixture.getSymbol(s) : s);
        return format == null ? realString : format.parseObject(realString);
    }

    public boolean isRecall() {
        return recall;
    }
}
