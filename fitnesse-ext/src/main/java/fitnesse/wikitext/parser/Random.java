package fitnesse.wikitext.parser;

import fitnesse.html.HtmlTag;
import util.Maybe;

public class Random extends SymbolType implements Rule, Translation {
    public static final String GENERATED_VALUE = "value";
    public static final String RAW_PARAMS = "raw";
    public static final String VAR_NAME = "variable";

    public Random() {
        super("Random");
        wikiMatcher(new Matcher().startLineOrCell().string("!random"));
        wikiRule(this);
        htmlTranslation(this);
    }

    public Maybe<Symbol> parse(Symbol current, Parser parser) {
        if (!parser.isMoveNext(SymbolType.Whitespace)) return Symbol.nothing;

        Maybe<String> name = parser.parseToAsString(SymbolType.OpenBrace);
        boolean setVar = !(name.isNothing() || "".equals(name.getValue().trim()));
        Maybe<String> value = parser.parseToAsString(SymbolType.CloseBrace);
        if (value.isNothing()) return Symbol.nothing;

        String rawParams = value.getValue();
        current.putProperty(RAW_PARAMS, rawParams);
        Long[] params = parseParameters(rawParams.split("\\s"), parser);
        String randomValue = random(params[0], params[1], params[2]);
        current.putProperty(GENERATED_VALUE, randomValue);
        if (randomValue == null) return Symbol.nothing;

        if (setVar) {
            String variableName = name.getValue().trim();
            if (!ScanString.isVariableName(variableName)) return Symbol.nothing;
            parser.getPage().putVariable(variableName, randomValue);
            current.putProperty(VAR_NAME, variableName);
        }

        return new Maybe<Symbol>(current);
    }

    private Long[] parseParameters(String[] rawParams, Parser parser) {
        Long[] toReturn = new Long[3];
        if (rawParams.length > 1) {
            toReturn[2] = 1L;
            for (int i = 0; i < rawParams.length; i++) {
                String param = rawParams[i];
                if (!param.matches("[-+]?[0-9]+")) {
                    //try getting a page variable:
                    if (ScanString.isVariableName(param)) {
                        param = parser.getVariableSource().findVariable(param).getValue();
                    }
                }
                try {
                    toReturn[i] = Long.parseLong(param);
                } catch (NumberFormatException e) {
                    toReturn[i] = null;
                }
            }
        }
        return toReturn;
    }

    private String random(Long high, Long low, Long factor) {
        if (high == null || low == null) {
            return null;
        }
        long result = (low + (long) (Math.random() * ((high - low) + 1))) * factor;
        return Long.toString(result);
    }

    public String toTarget(Translator translator, Symbol symbol) {
        String varName = symbol.getProperty(VAR_NAME);
        String generatedValue = symbol.getProperty(GENERATED_VALUE);
        HtmlTag result;
        if (varName == null || "".equals(varName)) {
            return generatedValue;
        } else {
            result = new HtmlTag("span", "random variable defined: "
                    + varName
                    + "="
                    + generatedValue);
        }
        result.addAttribute("class", "meta");
        return result.html();
    }
}
