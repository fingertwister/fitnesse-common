package au.net.thehardings.fitnesse.env;

import au.net.thehardings.fitnesse.util.DefaultEncryptionUtilImpl;
import au.net.thehardings.fitnesse.util.EncryptionUtil;
import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

import static au.net.thehardings.fitnesse.env.EnvBeanFactory.MAIN_CONFIG_LOCATION;

/**
 *
 */
public class DatabaseEnvironment {
    private static final Map<String, DatabaseEnvironment> VALUES = new HashMap<String, DatabaseEnvironment>();
    //even though this is not used, it's here to make sure we are properly initialised
    private static final EnvBeanFactory FACTORY = EnvBeanFactory.getInstance();

    EncryptionUtil util = new DefaultEncryptionUtilImpl();

    String driver;
    String url;
    String username;
    String password;

    public static DatabaseEnvironment factory(String env) {
        for (String key : VALUES.keySet()) {
            if (key.equalsIgnoreCase(env)) {
                return VALUES.get(key);
            }
        }
        throw new IllegalArgumentException("No DatabaseEnvironment bean is configured in " + MAIN_CONFIG_LOCATION + " for " + env);
    }

    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(util.decrypt(username, password));
        return dataSource;
    }

    public void setEnv(String env) {
        VALUES.put(env, this);
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUtil(EncryptionUtil util) {
        this.util = util;
    }
}
