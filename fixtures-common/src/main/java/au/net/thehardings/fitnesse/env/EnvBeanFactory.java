package au.net.thehardings.fitnesse.env;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This class allows us to use Spring injection when we don't have life-cycle hooks
 * to configure a bean factory appropriately.
 */
public final class EnvBeanFactory {

    public static final String MAIN_CONFIG_LOCATION = "environment-ctx.xml";

    private static final Logger LOG = LoggerFactory.getLogger(EnvBeanFactory.class);

    public static EnvBeanFactory getInstance() {
        return EnvBeanFactoryHolder.instance;
    }

    private EnvBeanFactory() {
        //env beans are self registering, so as long as the application
        //context is created, there is no need to keep a reference to it.
        new ClassPathXmlApplicationContext(MAIN_CONFIG_LOCATION);
    }

    // Initialisation holder on demand idiom
    private static class EnvBeanFactoryHolder {
        private static EnvBeanFactory instance = new EnvBeanFactory();
    }
}
