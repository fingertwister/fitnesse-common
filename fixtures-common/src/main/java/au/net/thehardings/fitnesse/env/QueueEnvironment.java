package au.net.thehardings.fitnesse.env;

import au.net.thehardings.fitnesse.util.DefaultEncryptionUtilImpl;
import au.net.thehardings.fitnesse.util.EncryptionUtil;

import javax.jms.QueueConnectionFactory;
import java.util.HashMap;
import java.util.Map;

import static au.net.thehardings.fitnesse.env.EnvBeanFactory.MAIN_CONFIG_LOCATION;

/**
 *
 */
public class QueueEnvironment {
    static final Map<String, QueueEnvironment> VALUES = new HashMap<String, QueueEnvironment>();
    //even though this is not used, it's here to make sure if we are initialised
    private static final EnvBeanFactory FACTORY = EnvBeanFactory.getInstance();

    QueueConnectionFactory connectionFactory;
    String username;
    String password;
    String dropName;
    EncryptionUtil util = new DefaultEncryptionUtilImpl();

    public static QueueEnvironment factory(String env) {
        for (String key : VALUES.keySet()) {
            if (key.equalsIgnoreCase(env)) {
                return VALUES.get(key);
            }
        }
        throw new IllegalArgumentException("No QueueEnvironment bean is configured in " + MAIN_CONFIG_LOCATION + " for " + env);
    }

    public String processQueueName(String queue) {
        if (queue == null || "".equals(queue)) {
            return null;
        }
        if (queue.lastIndexOf('.') == -1 || !(queue.substring(queue.lastIndexOf('.'), queue.length()).toUpperCase().startsWith(".DROP"))) {
            return queue.concat(dropName);
        } else {
            return queue;
        }
    }

    public void setEnv(String env) {
        VALUES.put(env, this);
    }

    public QueueConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public void setConnectionFactory(QueueConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return util.decrypt(username, password);
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDropName() {
        return dropName;
    }

    public void setDropName(String dropName) {
        this.dropName = dropName == null ? "" : dropName;
        if (!"".equals(this.dropName) && !this.dropName.startsWith(".")) {
            this.dropName = ".".concat(this.dropName);
        }
    }

    public void setUtil(EncryptionUtil util) {
        this.util = util;
    }
}
