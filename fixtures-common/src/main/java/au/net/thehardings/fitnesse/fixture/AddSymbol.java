package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.fixture.lifecycle.LifecycleColumnFixture;

/**
 * FIT syntax:
 * <p/>
 * <pre>
 * |add symbol             |
 * |symbolValue|=setSymbol?|
 * |Hello      |v          |
 * |World      |x          |
 * </pre>
 * <p/>
 * Slim syntax:
 * <p/>
 * <pre>
 * |add symbol            |
 * |symbolValue|setSymbol?|
 * |Hello      |$v=       |
 * |World      |$x=       |
 * </pre>
 */
public class AddSymbol extends LifecycleColumnFixture {
    String symbolValue;

    public void setSymbolValue(String newSymbolValue) {
        symbolValue = newSymbolValue;
    }

    public String setSymbol() {
        return symbolValue;
    }
}