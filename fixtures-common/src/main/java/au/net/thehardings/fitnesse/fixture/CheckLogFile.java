package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.fixture.lifecycle.FitToSlimUtil;
import au.net.thehardings.fitnesse.fixture.lifecycle.LifecycleColumnFixture;
import au.net.thehardings.fitnesse.util.StringUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * The fixture CheckLogFile expects a table in the format:
 * <p/>
 * <pre>
 * !|check log file  |inputFiles/server.log|yyyy-MM-dd HH:mm:ss|300|
 * |message          |inLog?                                       |
 * |Shutdown complete|true                                         |
 * </pre>
 */
public class CheckLogFile extends LifecycleColumnFixture {
    public static final String DEFAULT_LOOKBACK_SECONDS = "30";

    //utility classes
    protected FitToSlimUtil fitToSlim = new FitToSlimUtil();
    protected StringUtil util = new StringUtil();

    //fixture parameters
    protected String logFileName;
    protected String logDateFormat;
    protected int lookBackSeconds;

    //fixture variables
    String message;

    public CheckLogFile() {
    }

    public CheckLogFile(String logFileName, String logDateFormat) {
        configureParameters(logFileName, logDateFormat, DEFAULT_LOOKBACK_SECONDS);
    }

    public CheckLogFile(String logFileName, String logDateFormat, String lookBackSeconds) {
        configureParameters(logFileName, logDateFormat, lookBackSeconds);
    }

    /**
     * Initialise.
     */
    protected void initialiseFit() {
        // parse the table args for the check log file fixture
        String lfn = fitToSlim.getMandatoryArg("log file name", args, 1);
        String ldf = fitToSlim.getMandatoryArg("log date format", args, 2);
        String lbs = fitToSlim.getArg(args, 3, DEFAULT_LOOKBACK_SECONDS);
        configureParameters(lfn, ldf, lbs);
    }

    protected void configureParameters(String logFileName, String logDateFormat, String lookBackSeconds) {
        this.logFileName = logFileName;
        this.logDateFormat = logDateFormat;
        this.lookBackSeconds = Integer.parseInt(lookBackSeconds);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * In log.
     *
     * @return true, if successful
     * @throws Exception the exception
     */
    public boolean inLog() throws Exception {
        File logFile = new File(logFileName);
        if (!logFile.exists()) {
            throw new FileNotFoundException("The file at " + logFile.getAbsolutePath() + " could not be found.");
        }
        Scanner fileScanner = new Scanner(logFile);
        return checkLog(fileScanner);
    }

    protected boolean checkLog(Scanner s) {
        Scanner scanner = s.useDelimiter("\\n");
        // find the from date
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.SECOND, -lookBackSeconds);
        Date fromDate = calender.getTime();
        // get ready to parse
        DateFormat df = new SimpleDateFormat(logDateFormat);
        String logEntry;
        Date logDate = null;
        while (scanner.hasNext()) {
            logEntry = scanner.next();
            if (logEntry.length() >= logDateFormat.length()) {
                // this parser util expect the first log entry characters are
                // date stamp with time in format provided
                try {
                    logDate = df.parse(logEntry.substring(0, 19));
                } catch (ParseException pe) {
                    // ignore these
                }
            }
            if (logDate != null && logDate.after(fromDate)) {
                if (logEntry.contains(util.replaceSymbols(message))) {
                    return true;
                }
            }
        }
        return false;
    }
}
