package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.util.DefaultEncryptionUtilImpl;
import au.net.thehardings.fitnesse.util.EncryptionUtil;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * The fixture CheckRemoteLogFile expects a table in the format:
 * <p/>
 * <pre>
 * !|check log file  |user/password@server:/path/to/file|yyyy-MM-dd HH:mm:ss|900000000|
 * |message     |inLog?                                                                   |
 * |Shutdown complete|true                                                                     |
 * </pre>
 */
public class CheckRemoteLogFile extends CheckLogFile {
    private static final Logger LOG = LoggerFactory.getLogger(CheckRemoteLogFile.class);
    public static final byte[] HANDSHAKE = new byte[]{0};
    public static final byte NULL = 0x0a;
    public static final String DEFAULT_CHARSET = "UTF-8";
    public static final String KNOWN_HOSTS = "./FitNesseRoot/files/known_hosts";
    JSch jsch = new JSch();
    EncryptionUtil encUtil = new DefaultEncryptionUtilImpl();
    String user;
    String password;
    String host;
    String charset;

    public CheckRemoteLogFile() {
    }

    public CheckRemoteLogFile(String logFileName, String logDateFormat) {
        this(logFileName, logDateFormat, CheckLogFile.DEFAULT_LOOKBACK_SECONDS, DEFAULT_CHARSET);
    }

    public CheckRemoteLogFile(String logFileName, String logDateFormat, String lookBackSeconds) {
        this(logFileName, logDateFormat, lookBackSeconds, DEFAULT_CHARSET);
    }

    public CheckRemoteLogFile(String logFileName, String logDateFormat, String lookBackSeconds, String characterSet) {
        super(logFileName, logDateFormat, lookBackSeconds);
        configureParameters(characterSet);
    }

    @Override
    protected void initialiseFit() {
        super.initialiseFit();
        // superclass handles most of the parameters
        String characterSet = fitToSlim.getArg(args, 4, DEFAULT_CHARSET);
        configureParameters(characterSet);
    }

    void configureParameters(String characterSet) {
        charset = characterSet;

        //parse out the remote connection settings from the log file name
        if (logFileName.indexOf('/') > logFileName.indexOf('@')) {
            user = logFileName.substring(0, logFileName.indexOf('@'));
            logFileName = logFileName.substring(logFileName.indexOf('@') + 1);
            // assume the password is the same as the user.
            password = user;
        } else {
            user = logFileName.substring(0, logFileName.indexOf('/'));
            logFileName = logFileName.substring(logFileName.indexOf('/') + 1);
            password = encUtil.decrypt(user, logFileName.substring(0, logFileName.indexOf('@')));
            logFileName = logFileName.substring(logFileName.indexOf('@') + 1);
        }
        host = logFileName.substring(0, logFileName.indexOf(':'));
        logFileName = util.replaceSymbols(logFileName.substring(logFileName.indexOf(':') + 1));
    }

    public boolean fileExists() throws Exception {
        Session session = getSession();
        session.connect();
        try {
            // exec 'scp -f fileName' remotely
            String command = "scp -f " + logFileName;
            ChannelExec channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(command);

            // get I/O streams for remote scp
            OutputStream out = channel.getOutputStream();
            InputStream in = channel.getInputStream();
            channel.connect();

            //start the ssh handshake
            try {
                if (!checkAck(in, out)) {
                    return false;
                }
            } catch (RuntimeException e) {
                if (e.getLocalizedMessage().contains("No such file or directory")) {
                    return false;
                } else {
                    throw e;
                }
            }

            // read '0644 '
            in.read(new byte[5]);

            // read the file size
            getFileSize(in, out);
        } finally {
            session.disconnect();
        }
        return true;
    }

    /**
     * In log.
     *
     * @return true, if successful
     * @throws Exception the exception
     */
    public boolean inLog() throws Exception {
        boolean result = false;
        Session session = getSession();
        session.connect();
        try {
            // exec 'scp -f logFileName' remotely
            String command = "scp -f " + logFileName;
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            // get I/O streams for remote scp
            OutputStream out = channel.getOutputStream();
            InputStream in = channel.getInputStream();
            channel.connect();

            if (!checkAck(in, out)) {
                return false;
            }

            // read '0644 '
            in.read(new byte[5]);

            // read the file size
            long fileSize = getFileSize(in, out);
            String fileName = getFileName(in, out);

            //now we are up to the file content
            LOG.debug("Scanning {}.", fileName);
            Scanner scanner = new Scanner(in, charset);
            result = checkLog(scanner, fileSize);

            //finally finish the scp handshake with the output stream by sending '\0'
            out.write(HANDSHAKE);
            out.flush();
        } finally {
            session.disconnect();
        }
        return result;
    }

    protected boolean checkLog(Scanner s, long fileSize) {
        Scanner scanner = s.useDelimiter("\\n");
        // find the from date
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.SECOND, -lookBackSeconds);
        Date fromDate = calender.getTime();
        // get ready to parse
        DateFormat df = new SimpleDateFormat(logDateFormat);
        String logEntry;
        Date logDate = null;
        long byteCount = 0L;

        while (scanner.hasNext() && fileSize > byteCount) {
            logEntry = scanner.next();
            //need to add an extra 1 for the \n scanner delimiter that gets dropped
            byteCount += (logEntry.length() + 1);
            LOG.debug("Got {} bytes of {}.", byteCount, fileSize);
            if (logEntry.length() >= logDateFormat.length()) {
                // this parser util expect the first log entry characters are
                // date stamp with time in format provided
                try {
                    logDate = df.parse(logEntry.substring(0, 19));
                } catch (ParseException pe) {
                    // ignore these
                }
            }
            if (logDate != null && logDate.after(fromDate)) {
                if (logEntry.contains(util.replaceSymbols(message))) {
                    return true;
                }
            }
        }
        return false;
    }

    Session getSession() throws JSchException {
        jsch.setKnownHosts(KNOWN_HOSTS);
        Session session = jsch.getSession(user, host, 22);
        session.setPassword(password);

        // We make sure that we don't need complete names to connect to the server
        java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        return session;
    }

    boolean checkAck(InputStream in, OutputStream out) throws IOException {
        out.write(HANDSHAKE);
        out.flush();
        int b = in.read();
        // b may be C for success,
        // 1 for error,
        // 2 for fatal error,
        // -1 for nothing read
        if (b == 1 || b == 2) {
            StringBuffer sb = new StringBuffer();
            int c = in.read();
            while (c != '\n') {
                sb.append((char) c);
                c = in.read();
            }
            throw new RuntimeException(sb.toString());
        }

        return b == 'C';
    }

    long getFileSize(InputStream in, OutputStream out) throws IOException {
        byte[] buf = new byte[1];
        // read the file size
        long fileSize = 0L;
        while (true) {
            if (in.read(buf) < 0) {
                throw new RuntimeException("An error occurred retrieving the file size");
            }
            if (buf[0] == ' ') {
                break;
            }
            fileSize = fileSize * 10L + (long) (buf[0] - '0');
        }
        LOG.debug("File size is {}.", fileSize);

        //handshake the file size
        out.write(HANDSHAKE);
        out.flush();
        return fileSize;
    }

    String getFileName(InputStream in, OutputStream out) throws IOException {
        byte[] buf = new byte[1];
        // read the remote file name
        List<Byte> bytes = new ArrayList<Byte>();
        while (true) {
            in.read(buf);
            if (buf[0] == NULL) {
                break;
            }
            bytes.add(buf[0]);
        }
        byte[] name = new byte[bytes.size()];
        for (int i = 0; i < bytes.size(); i++) {
            Byte b = bytes.get(i);
            name[i] = b;
        }
        String fileName = new String(name, "UTF-8");
        LOG.debug("File name is {}.", fileName);

        // send '\0' again
        out.write(HANDSHAKE);
        out.flush();
        return fileName;
    }
}
