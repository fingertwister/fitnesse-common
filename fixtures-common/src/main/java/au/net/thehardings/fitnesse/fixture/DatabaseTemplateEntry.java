package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.transport.JdbcTransport;
import au.net.thehardings.fitnesse.util.StringUtil;
import freemarker.template.Template;

import java.io.StringReader;

/**
 * The class <code>DatabaseTemplateEntry</code> processes a row from the fixture
 * table and loads it into a Velocity map, using the column titles as the
 * variable names.
 * <p/>
 * The arguments are:
 * <ol>
 * <li>Environment Name</li>
 * <li>SQL to execute</li>
 * <li>Optional DEBUG flag</li>
 * </ol>
 * <p/>
 * An example of this fixture defined in FIT Syntax is:
 * <p/>
 * <pre>
 * !|database template entry|DEV|insert into test values (!-${id}, '${name}'-!)|
 * |id                      |name                                              |
 * |1                       |Noddy                                             |
 * |2                       |Big Ears                                          |
 * </pre>
 * <p/>
 * An example of this fixture defined in SLiM Syntax is:
 * <p/>
 * <pre>
 * !|map:database template entry|DEV|insert into test values (!-${id}, '${name}'-!)|
 * |id                          |name                                              |
 * |1                           |Noddy                                             |
 * |2                           |Big Ears                                          |
 * </pre>
 * Note that the fixture uses the Map SLiM table, which is a custom SLiM table that needs to be added to the
 * plugin.properties file to be available for use. See {@link fitnesse.testsystems.slim.tables.MapTable MapTable}
 * for more details.
 * <p/>
 * Note that Freemarker variable syntax is the same as FitNesse variable syntax, so
 * Freemarker variables to be rendered by the template will need to be block escaped
 * with FitNesse escape !- and -!
 * <p/>
 * This would then in turn be rendered once for each of the rows in the fixture
 * - their values being substituted in places of the corresponding
 * ${variable-name} placeholders and the resultant sql executed against the
 * database provided by the arguments to the fixture.
 */
public class DatabaseTemplateEntry extends TemplateEntryFixture {
    //utility classes
    protected StringUtil util = new StringUtil();

    //fixture parameters
    JdbcTransport transport = new JdbcTransport();

    public DatabaseTemplateEntry() {
    }

    public DatabaseTemplateEntry(String env, String sql) {
        //SLiM runners put "<br/>" in test variables when they span a line.
        String cleanSql = util.stripHtmlLineBreaks(sql);

        configureParameters(env, cleanSql);
    }

    @Override
    protected void initialiseFit() {
        //the super implementation expects the first arg to be template name. But here we are going
        //to build the template up based on the SQL we receive, so we don't call to super.initialiseFit()
        String env = fitToSlim.getMandatoryArg("environment", args, 1);
        String sql = fitToSlim.getMandatoryArg("sql", args, 2);

        //FIT runners don't symbol replace on parameters
        sql = util.replaceSymbols(sql);

        configureParameters(env, sql);
    }

    void configureParameters(String env, String sql) {
        transport.setEnv(env);
        StringReader reader = new StringReader(sql);
        try {
            template = new Template("DatabaseTemplateEntryTemplate", reader, cfg);
        } catch (Exception e) {
            //Give a little context and re-throw
            throw new RuntimeException("An error occurred while trying to create the sql template.", e);
        }
    }

    protected void send(String content) {
        transport.sendMessage(content);
    }
}
