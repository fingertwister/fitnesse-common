package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.fixture.lifecycle.FitToSlimUtil;
import au.net.thehardings.fitnesse.fixture.lifecycle.LifecycleColumnFixture;
import au.net.thehardings.fitnesse.transport.JdbcTransport;
import au.net.thehardings.fitnesse.util.StringUtil;

import java.util.Date;

/**
 * The class <code>DatabaseValueSelect</code> runs the select provided in the
 * fixture and can return the SINGLE value of the SINGLE row returned.
 * <p/>
 * The arguments are:
 * <ol>
 * <li>Environment Name</li>
 * <li>SQL to execute. The SQL must select ONLY ONE column, and ONLY ONE row
 * must be returned</li>
 * </ol>
 * <p/>
 * Here is an example of this fixture defined in SLiM Syntax:
 * <p/>
 * <pre>
 * |database value select|DEV|select name from test where id = $x|
 * |value?                                                       |
 * |$myValue=                                                    |
 * </pre>
 * <p/>
 * Here is an example of this fixture defined in FIT Syntax:
 * <p/>
 * <pre>
 * |database value select|DEV|select name from test where id = $[x]|
 * |=value?                                                        |
 * |myValue                                                        |
 * </pre>
 * <p/>
 * In this case the symbols <i>'$x'/'x'</i> have already been defined in an earlier part
 * of the script. After this script runs the symbols <i>'$myValue'/'myValue'</i> will
 * be populated with the value of the <i>name</i> column where <i>id = '$x'/'x'</i>.
 */
public class DatabaseValueSelect extends LifecycleColumnFixture {
    //utility classes
    StringUtil util = new StringUtil();
    FitToSlimUtil fitToSlim = new FitToSlimUtil();

    //fixture parameters
    JdbcTransport transport = new JdbcTransport();
    String query;

    public DatabaseValueSelect() {
    }

    public DatabaseValueSelect(String env, String queryString) {
        //SLiM runners put "<br/>" in test variables when they span a line.
        String cleanQueryString = util.stripHtmlLineBreaks(queryString);
        cleanQueryString = util.replaceSymbols(cleanQueryString);

        configureParameters(env, cleanQueryString);
    }

    protected void initialiseFit() {
        String env = fitToSlim.getMandatoryArg("environment", args, 1);
        String queryString = fitToSlim.getMandatoryArg("query", args, 2);

        //FIT runners don't symbol replace on parameters
        queryString = util.replaceSymbols(queryString);

        configureParameters(env, queryString);
    }

    void configureParameters(String env, String queryString) {
        transport.setEnv(env);
        query = queryString;
    }

    public Object value() {
        return query();
    }

    public String strValue() {
        return (String) query();
    }

    public Long longValue() {
        return (Long) query();
    }

    public Date dateValue() {
        return (Date) query();
    }

    Object query() {
        return transport.queryForObject(query);
    }
}
