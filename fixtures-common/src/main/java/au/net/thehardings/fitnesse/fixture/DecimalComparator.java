package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.fixture.lifecycle.LifecycleColumnFixture;

/**
 * Simple utility fixture that allows for number comparisons on decimals
 */
public class DecimalComparator extends LifecycleColumnFixture {
    double field;

    public void setField(double field) {
        this.field = field;
    }

    public double compare() {
        return field;
    }
}
