package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.transport.FileTransport;

/**
 * The class <code>FileTemplateEntry</code> processes a row from the fixture table
 * and loads it into a Velocity map, using the column titles as the variable
 * names. The arguments are:
 * <ol>
 * <li>Template Name and Path (<b><i>mandatory</i></b> String) - relative to the Fitnesse files directory</li>
 * <li>File Name (<b><i>mandatory</i></b> String) - using a {0} in the file name will be replaced with a three digit number that increments
 * from zero if overwrite is true, otherwise from last number on the filesystem</li>
 * <li>Overwrite (<b><i>optional</i></b> boolean, defaults to "false") - will overwrite files with the same name when true</li>
 * </ol>
 * <p/>
 * An example of this fixture defined in FIT Syntax is:
 * <pre>
 * !|file template entry|XML/test.xml|C:\temp\test-{0}.xml|
 * |templateVar1|templateVar2|templateVar3|templateVar4|templateVar5|templateVar6|templateVar7|
 * |value-a1|value-a2|value-a3|value-a4|value-a5|value-a6|value-a7|
 * |value-b1|value-b2|value-b3|value-b4|value-b5|value-b6|value-b7|
 * |value-c1|value-c2|value-c3|value-c4|value-c5|value-c6|value-c7|
 * </pre>
 * <p/>
 * An example of this fixture defined in SLiM Syntax is:
 * <pre>
 * !|map:file template entry|XML/test.xml|C:\temp\test-{0}.xml|
 * |templateVar1|templateVar2|templateVar3|templateVar4|templateVar5|templateVar6|templateVar7|
 * |value-a1|value-a2|value-a3|value-a4|value-a5|value-a6|value-a7|
 * |value-b1|value-b2|value-b3|value-b4|value-b5|value-b6|value-b7|
 * |value-c1|value-c2|value-c3|value-c4|value-c5|value-c6|value-c7|
 * </pre>
 * Note that the fixture uses the Map SLiM table, which is a custom SLiM table that needs to be added to the
 * plugin.properties file to be available for use. See {@link fitnesse.testsystems.slim.tables.MapTable MapTable}
 * for more details.
 * <p/>
 * To use this example a file would need to be defined to use the template
 * variables in freemarker syntax (see <a href="http://freemarker.org/">Freemarker's
 * homepage</a> for more information. For example, a simple template based on the above
 * FitNesse fixture definition would be:
 * <p/>
 * <pre>
 * &lt;xml&gt;
 *   &lt;body&gt;
 *     &lt;value1&gt;${templateVar1}&lt;/value1&gt;
 *     &lt;value2&gt;${templateVar2}&lt;/value2&gt;
 *     &lt;value3&gt;${templateVar3}&lt;/value3&gt;
 *     &lt;value4&gt;${templateVar4}&lt;/value4&gt;
 *     &lt;value5&gt;${templateVar5}&lt;/value5&gt;
 *     &lt;value6&gt;${templateVar6}&lt;/value6&gt;
 *     &lt;value7&gt;${templateVar7}&lt;/value7&gt;
 *   &lt;/body&gt;
 * &lt;/xml&gt;
 * </pre>
 * <p/>
 * This would then in turn be rendered once for each of the rows in the fixture -
 * their values being substituted in places of the corresponding ${variable-name}
 * placeholders and the resultant message placed on the queue provided by the
 * arguments to the fixture.
 */
public class FileTemplateEntry extends TemplateEntryFixture {
    FileTransport transport = new FileTransport();

    public FileTemplateEntry() {
    }

    public FileTemplateEntry(String templateFile, String fileName) {
        this(templateFile, fileName, false);
    }

    public FileTemplateEntry(String templateFile, String fileName, boolean overwrite) {
        super(templateFile);
        configureParameters(fileName, overwrite);
    }

    protected void initialiseFit() {
        super.initialiseFit();

        //the first arg (template name) is handled in the super implementation
        String fileName = fitToSlim.getMandatoryArg("template file", args, 2);
        String overwrite = fitToSlim.getArg(args, 3, "false");
        configureParameters(fileName, Boolean.parseBoolean(overwrite));
    }

    void configureParameters(String fileName, boolean overwrite) {
        transport.setFileName(fileName);
        transport.setOverwrite(overwrite);
    }

    /**
     * Publishes a Given Message.
     *
     * @param content The message to publish
     * @throws Exception when an error occurs
     */
    protected void send(String content) {
        transport.sendMessage(content);
    }
}