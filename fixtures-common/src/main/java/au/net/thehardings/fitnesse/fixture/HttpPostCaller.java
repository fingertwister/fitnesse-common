package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.fixture.lifecycle.LifecycleColumnFixture;
import au.net.thehardings.fitnesse.transport.HttpTransport;

import java.net.URLEncoder;

/**
 * A column fixture that makes HttpPost calls directed to the given targetURL
 * with the given urlParameters
 *
 * @author a478514
 */
public class HttpPostCaller extends LifecycleColumnFixture {
    /**
     * The targetURL.
     */
    String targetURL;
    /**
     * The comma separated key value pair of parameters.
     * key1=value1,key2=value2,...keyN=valueN
     */
    String urlParameters;
    /**
     * The transport mechanism to make the call to targetURL
     */
    HttpTransport transport = new HttpTransport();

    public HttpPostCaller() {
    }

    /**
     * @return Returns the response of the http post request
     */
    public String call() throws Exception {
        return transport.executePost(targetURL, getEncodedUrlParameters(urlParameters));
    }

    public void setTargetURL(String targetURL) {
        this.targetURL = targetURL;
    }

    public void setUrlParameters(String urlParameters) {
        this.urlParameters = urlParameters;
    }

    String getEncodedUrlParameters(String urlParams) throws Exception {
        String encodedUrlParameters = "";
        String[] keyValuePair = null;
        String[] keyValuePairArray = urlParams.split(",");
        for (int indx = 0; indx < keyValuePairArray.length; indx++) {
            keyValuePair = keyValuePairArray[indx].split("=");
            if (indx == 0) {
                encodedUrlParameters = URLEncoder.encode(keyValuePair[0], "UTF-8") + "=" + URLEncoder.encode(keyValuePair[1], "UTF-8");
            } else {
                encodedUrlParameters += "&" + URLEncoder.encode(keyValuePair[0], "UTF-8") + "=" + URLEncoder.encode(keyValuePair[1], "UTF-8");
            }
        }
        return encodedUrlParameters;
    }
}
