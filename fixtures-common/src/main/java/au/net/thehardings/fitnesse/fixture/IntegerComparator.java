package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.fixture.lifecycle.LifecycleColumnFixture;

/**
 * Simple utility fixture that allows for number comparisons on integers
 */
public class IntegerComparator extends LifecycleColumnFixture {
    int field;

    public void setField(int field) {
        this.field = field;
    }

    public int compare() {
        return field;
    }
}
