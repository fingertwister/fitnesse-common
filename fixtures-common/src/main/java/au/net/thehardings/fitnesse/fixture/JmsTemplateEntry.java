package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.transport.JmsTransport;

/**
 * The class <code>JmsTemplateEntry</code> processes a row from the fixture table
 * and loads it into a Velocity context, using the column titles as the variable
 * names. The arguments are:
 * <ol>
 * <li>Template Name and Path (from the Fitnesse files directory)</li>
 * <li>Environment</li>
 * <li>Queue Name</li>
 * <li>JMS Message Type - optional (defaults to BYTES)</li>
 * <li>Charset Name - optional (defaults to UTF-8)</li>
 * </ol>
 * <p/>
 * An example of this fixture defined in Wiki Syntax is:
 * <pre>
 * !|jms template entry|XML/test.xml|DEV|MY.QUEUE|
 * |templateVar1|templateVar2|templateVar3|templateVar4|templateVar5|templateVar6|templateVar7|
 * |value-a1|value-a2|value-a3|value-a4|value-a5|value-a6|value-a7|
 * |value-b1|value-b2|value-b3|value-b4|value-b5|value-b6|value-b7|
 * |value-c1|value-c2|value-c3|value-c4|value-c5|value-c6|value-c7|
 * </pre>
 * <p/>
 * To use this example a file would need to be defined to use the template
 * variables in Freemarker syntax (see <a href="http://freemarker.org">Freemarker's
 * homepage</a> for more information. For example, a simple template based on the above
 * FitNesse fixture definition would be:
 * <p/>
 * <pre>
 * &lt;xml&gt;
 *   &lt;body&gt;
 *     &lt;value1&gt;${templateVar1}&lt;/value1&gt;
 *     &lt;value2&gt;${templateVar2}&lt;/value2&gt;
 *     &lt;value3&gt;${templateVar3}&lt;/value3&gt;
 *     &lt;value4&gt;${templateVar4}&lt;/value4&gt;
 *     &lt;value5&gt;${templateVar5}&lt;/value5&gt;
 *     &lt;value6&gt;${templateVar6}&lt;/value6&gt;
 *     &lt;value7&gt;${templateVar7}&lt;/value7&gt;
 *   &lt;/body&gt;
 * &lt;/xml&gt;
 * </pre>
 * <p/>
 * This would then in turn be rendered once for each of the rows in the fixture -
 * their values being substituted in places of the corresponding ${variable-name}
 * placeholders and the resultant message placed on the queue provided by the
 * arguments to the fixture.
 */
public class JmsTemplateEntry extends TemplateEntryFixture {

    public static final String DEFAULT_MSG_TYPE = "BYTES";
    public static final String DEFAULT_CHARSET = "UTF-8";

    enum JmsMessageType {
        BYTES, TEXT
    }

    JmsTransport transport = new JmsTransport();
    JmsMessageType messageType;
    String charsetName;

    public JmsTemplateEntry() {
    }

    public JmsTemplateEntry(String templateFile, String env, String queue) {
        this(templateFile, env, queue, DEFAULT_MSG_TYPE);
    }

    public JmsTemplateEntry(String templateFile, String env, String queue, String msgType) {
        this(templateFile, env, queue, msgType, DEFAULT_CHARSET);
    }

    public JmsTemplateEntry(String templateFile, String env, String queue, String msgType, String charset) {
        super(templateFile);
        configureParameters(env, queue, msgType, charset);
    }

    protected void initialiseFit() {
        super.initialiseFit();

        //the first arg (template name) is handled in the super implementation
        String env = fitToSlim.getMandatoryArg("environment", args, 2);
        String queue = fitToSlim.getMandatoryArg("queue", args, 3);
        String msgType = fitToSlim.getArg(args, 4, DEFAULT_MSG_TYPE);
        String charset = fitToSlim.getArg(args, 5, DEFAULT_CHARSET);
        configureParameters(env, queue, msgType, charset);
    }

    void configureParameters(String env, String queue, String msgType, String charset) {
        transport.setEnv(env);
        transport.setQueue(queue);
        messageType = JmsMessageType.valueOf(msgType);
        charsetName = charset;
    }

    protected void send(String content) {
        switch (messageType) {
        case BYTES:
            transport.sendBytesMessage(content, charsetName);
            break;
        case TEXT:
            transport.sendMessage(content);
            break;
        }
    }
}
