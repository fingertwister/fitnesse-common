package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.fixture.lifecycle.FitToSlimUtil;
import au.net.thehardings.fitnesse.fixture.lifecycle.LifecycleFixture;

import java.util.List;

/**
 * A simple brute force approach to giving the system time to complete the underlying
 * work before progressing with the test case. The value parameter is specified in
 * milliseconds.
 * <p/>
 * This fixture supports both FIT and SLiM test runners. An example of this fixture
 * defined in Wiki Syntax is:
 * <pre>
 * |pause script|1000|
 * </pre>
 * This would halt the test script from continuing for one second.
 */
public class PauseScript extends LifecycleFixture {
    //utility classes
    protected FitToSlimUtil fitToSlim = new FitToSlimUtil();
    
    long millis = 0;

    /**
     * Constructor for FIT
     */
    public PauseScript() {
    }

    /**
     * Constructor for SLiM
     *
     * @param millis number of millis to pause
     */
    public PauseScript(long millis) {
        this.millis = millis;
    }

    /**
     * Initialise the FIT fixture from the table arguments
     */
    protected void initialiseFit() {
        // parse the table args for the pause fixture
        millis = Long.parseLong(fitToSlim.getMandatoryArg("millis", args, 1));
    }

    /**
     * Pause the specified number of millis
     *
     * @param table The table details
     */
    @Override
    public void table(List<List<String>> table) {
        //wait for the configured period
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            //best attempt
        }
    }
}
