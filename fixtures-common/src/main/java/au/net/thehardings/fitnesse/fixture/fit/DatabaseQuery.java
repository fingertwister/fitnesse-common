package au.net.thehardings.fitnesse.fixture.fit;

import au.net.thehardings.fitnesse.env.DatabaseEnvironment;
import au.net.thehardings.fitnesse.fixture.lifecycle.FitToSlimUtil;
import au.net.thehardings.fitnesse.util.StringUtil;
import fit.Binding;
import fit.Fixture;
import fit.MapTypeAdapter;
import fit.Parse;
import fit.RowFixture;
import fit.TypeAdapter;
import fit.exception.FitFailureException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.Format;
import java.util.List;
import java.util.Map;

public class DatabaseQuery extends RowFixture {
    DatabaseEnvironment env;
    String query;
    FitToSlimUtil fitToSlim = new FitToSlimUtil();
    StringUtil util = new StringUtil();
    JdbcTemplate template = new JdbcTemplate();

    @Override
    public void doTable(Parse table) {
        initialise();
        super.doTable(table);
    }

    void initialise() {
        //parse the table args
        env = DatabaseEnvironment.factory(fitToSlim.getMandatoryArg("environment", args, 1));
        query = fitToSlim.getMandatoryArg("sql", args, 2);
        query = util.replaceSymbols(query);
    }

    @Override
    public Object[] query() throws Exception {
        //build the jdbc template
        template.setDataSource(env.getDataSource());

        //execute the query
        List<Map<String, Object>> toReturn = template.queryForList(query);
        return toReturn.toArray(new Object[toReturn.size()]);
    }

    @Override
    public Class<?> getTargetClass() {
        return Map.class;
    }

    /**
     * This needs to be overridden to alter the text for recall columns.
     * Note the case of the symbol not being found in the Symbol Table is handled in MapTypeAdapter directly.
     */
    @Override
    public void check(Parse cell, TypeAdapter a) {
        String cellBody = cell.text();
        super.check(cell, a);
        //now alter the text if we are a recall column.
        if (((MapTypeAdapter) a).isRecall()) {
            String value = (String) Fixture.getSymbol(cellBody);
            cell.addToBody(Fixture.gray(" = " + value));
        }
    }

    @Override
    protected Binding createBinding(int column, Parse heads) {
        Binding binding = new Binding.QueryBinding();
        binding.adapter = new MapTypeAdapter((Map<Class<?>, Format>) null, heads.text());
        return binding;
    }
}
