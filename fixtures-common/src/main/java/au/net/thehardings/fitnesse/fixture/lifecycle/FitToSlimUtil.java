package au.net.thehardings.fitnesse.fixture.lifecycle;

import fit.Parse;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple swiss army knife utility that provides support for
 * making FIT and SLiM play nice together to reduce code overall.
 */
public class FitToSlimUtil {

    public List<List<String>> parseFitTable(Parse table) {
        //skip the first row which contains the fixture and parameters
        List<List<String>> toReturn = new ArrayList<List<String>>();
        for (Parse row = table.parts.more; row != null; row = row.more) {
            List<String> tableRow = new ArrayList<String>();
            for (Parse cell = row.parts; cell != null; cell = cell.more) {
                tableRow.add(cell.body);
            }
            toReturn.add(tableRow);
        }
        return toReturn;
    }

    public String getArg(String[] args, int index) {
        return getArg(args, index, null);
    }

    public String getArg(String[] args, int index, String defaultValue) {
        return (args != null && args.length >= index) ? args[index - 1] : defaultValue;
    }

    public String getMandatoryArg(String name, String[] args, int index) {
        String value = getArg(args, index);
        if (value == null) {
            throw new IllegalArgumentException("The number " + index + " fixture parameter ('" + name + "') was not supplied.");
        }
        return value;
    }
}
