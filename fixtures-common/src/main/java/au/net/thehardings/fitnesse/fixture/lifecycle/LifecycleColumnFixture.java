package au.net.thehardings.fitnesse.fixture.lifecycle;

import fit.ColumnFixture;
import fit.Parse;

import java.util.List;

/**
 * The LifecycleColumnFixture pushes adds support for the SLiM
 * Lifecycle methods to the FIT ColumnFixture class.
 */
public abstract class LifecycleColumnFixture extends ColumnFixture {

    FitToSlimUtil fitToSlim = new FitToSlimUtil();

    /**
     * {@inheritDoc}
     * Hook in the SLiM lifecycle method to the FIT doTable method. This allows for
     * one fixture to support both test runners.
     *
     * @see fit.Fixture#doTable(fit.Parse)
     */
    @Override
    public void doTable(Parse parse) {
        initialiseFit();
        table(fitToSlim.parseFitTable(parse));
        super.doTable(parse);
    }

    /**
     * {@inheritDoc}
     * Hook in the SLiM lifecycle methods to the FIT doRows method. This allows for
     * one fixture to support both test runners.
     *
     * @see fit.Fixture#doRows(fit.Parse)
     */
    @Override
    public void doRows(Parse rows) {
        beginTable();
        super.doRows(rows);
        endTable();
    }

    /**
     * Initialise the Fixture. This is where FIT table parameters are set.
     */
    protected void initialiseFit() {
    }

    /*
     * These next methods are the SLiM LifeCycle methods
     */

    public void table(List<List<String>> table) {
    }

    public void beginTable() {
    }

    /**
     * FIT has this lifecycle method as well, but it does nothing.
     */
    @Override
    public void execute() {
    }

    /**
     * FIT has this lifecycle method as well, but it does nothing.
     */
    @Override
    public void reset() {
    }

    public void endTable() {
    }
}
