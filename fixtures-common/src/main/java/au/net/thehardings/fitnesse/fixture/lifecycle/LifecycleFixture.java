package au.net.thehardings.fitnesse.fixture.lifecycle;

import fit.Fixture;
import fit.Parse;

import java.util.List;

/**
 * The fixture CheckLogFile expects a table in the format:
 * <p/>
 * <pre>
 * !|check log file  |./FitNesseRoot/files/inputFiles/server.log|yyyy-MM-dd HH:mm:ss|900000000|
 * |message          |inLog?                                                                  |
 * |Shutdown complete|true                                                                    |
 * </pre>
 */
public abstract class LifecycleFixture extends Fixture {

    FitToSlimUtil fitToSlim = new FitToSlimUtil();

    /**
     * {@inheritDoc}
     * Hook in the SLiM lifecycle methods to the FIT doTable method. This allows for
     * one fixture to support both test runners.
     *
     * @see fit.Fixture#doTable(fit.Parse)
     */
    @Override
    public void doTable(Parse parse) {
        initialiseFit();
        table(fitToSlim.parseFitTable(parse));
        super.doTable(parse);
    }

    /**
     * {@inheritDoc}
     * Hook in the SLiM lifecycle methods to the FIT doRows method. This allows for
     * one fixture to support both test runners.
     *
     * @see fit.Fixture#doRows(fit.Parse)
     */
    @Override
    public void doRows(Parse rows) {
        beginTable();
        super.doRows(rows);
        endTable();
    }

    /**
     * {@inheritDoc}
     * Hook in the SLiM lifecycle methods to the FIT doRow method. This allows for
     * one fixture to support both test runners.
     *
     * @see fit.Fixture#doRow(fit.Parse)
     */
    @Override
    public void doRow(Parse row) {
        super.doRow(row);
        execute();
        reset();
    }

    /**
     * Initialise the Fixture. This is where FIT table parameters are set.
     */
    protected void initialiseFit() {
    }

    /*
     * These next methods are the SLiM LifeCycle methods
     */

    public void table(List<List<String>> table) {
    }

    public void execute() {
    }

    public void reset() {
    }

    public void beginTable() {
    }

    public void endTable() {
    }
}
