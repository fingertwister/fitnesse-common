package au.net.thehardings.fitnesse.fixture.lifecycle;

import fit.Parse;
import fitnesse.fixtures.RowEntryFixture;

import java.util.List;

/**
 * The fixture CheckLogFile expects a table in the format:
 * <p/>
 * <pre>
 * !|check log file  |./FitNesseRoot/files/inputFiles/server.log|yyyy-MM-dd HH:mm:ss|900000000|
 * |message          |inLog?                                                                  |
 * |Shutdown complete|true                                                                    |
 * </pre>
 */
public abstract class LifecycleRowEntryFixture extends RowEntryFixture {

    FitToSlimUtil fitToSlim = new FitToSlimUtil();

    /**
     * {@inheritDoc}
     * Hook in the SLiM lifecycle methods to the FIT doTable method. This allows for
     * one fixture to support both test runners.
     *
     * @see fit.Fixture#doTable(fit.Parse)
     */
    @Override
    public void doTable(Parse parse) {
        initialiseFit();
        table(fitToSlim.parseFitTable(parse));
        super.doTable(parse);
    }

    /**
     * {@inheritDoc}
     * Hook in the SLiM lifecycle methods to the FIT doRows method. This allows for
     * one fixture to support both test runners.
     *
     * @see fit.Fixture#doRows(fit.Parse)
     */
    @Override
    public void doRows(Parse rows) {
        beginTable();
        super.doRows(rows);
        endTable();
    }

    /**
     * This is a RowEntryFixture, so the enterRow method must be defined, but this adds
     * little benefit over execute since both are done at approximately the same point
     * in the table lifecycle. Since SLiM uses the execute method, that should be the
     * preferred implementation for sub-classes, and so the abstract enterRow method is
     * stubbed out here.
     * <p/>
     * NOTE: While it is possible to call execute() from enterRow(), that would effectively
     * result in the execute method being called twice - once for the execute lifecycle call
     * which still exists in the RowEntryFixture, and the other for the enterRow lifecycle call.
     */
    public void enterRow() {
    }

    /**
     * Initialise the Fixture. This is where FIT table parameters are set.
     */
    protected void initialiseFit() {
    }

    /*
     * These next methods are the SLiM LifeCycle methods
     */

    public void table(List<List<String>> table) {
    }

    public void beginTable() {
    }

    /**
     * FIT has this lifecycle method as well, but it does nothing.
     */
    @Override
    public void execute() {
    }

    /**
     * FIT has this lifecycle method as well, but it does nothing.
     */
    @Override
    public void reset() {
    }

    public void endTable() {
    }
}
