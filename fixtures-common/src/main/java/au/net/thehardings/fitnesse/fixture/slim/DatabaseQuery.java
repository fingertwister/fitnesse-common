package au.net.thehardings.fitnesse.fixture.slim;

import au.net.thehardings.fitnesse.env.DatabaseEnvironment;
import au.net.thehardings.fitnesse.util.StringUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import util.ListUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DatabaseQuery {
    DatabaseEnvironment env;
    String query;
    StringUtil util = new StringUtil();
    JdbcTemplate template = new JdbcTemplate();

    public DatabaseQuery(String environment, String sql) {
        env = DatabaseEnvironment.factory(environment);
        query = sql;
        query = util.stripHtmlLineBreaks(query);
    }

    public List query() throws Exception {
        //build the jdbc template
        template.setDataSource(env.getDataSource());

        //execute the query
        return convertMapsToLists(template.queryForList(query));
    }

    List convertMapsToLists(List<Map<String, Object>> results) {
        List<List<List<Object>>> list = new ArrayList<List<List<Object>>>();
        for (Map<String, Object> result : results) {
            List<List<Object>> row = new ArrayList<List<Object>>();
            for (String key : result.keySet()) {
                row.add(ListUtility.list(key, result.get(key)));
            }
            list.add(row);
        }
        return list;
    }
}
