package au.net.thehardings.fitnesse.fixture.slim.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.android.AndroidDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.iphone.IPhoneDriver;
import org.openqa.selenium.iphone.IPhoneSimulatorDriver;
import org.openqa.selenium.safari.SafariDriver;

/**
 * A simple enumeration that provides an implementation of the Selenium WebDriver interface.
 */
public enum WebDriverFactory {
    FIREFOX("Firefox", FirefoxDriver.class),
    ANDROID("Android", AndroidDriver.class),
    INTERNET_EXPLORER("Internet Explorer", InternetExplorerDriver.class),
    CHROME("Chrome", ChromeDriver.class),
    SAFARI("Safari", SafariDriver.class),
    IPHONE("iPhone", IPhoneDriver.class),
    HTML_UNIT("HTML Unit", HtmlUnitDriver.class),
    IPHONE_SIMULATOR("iPhone Simulator", IPhoneSimulatorDriver.class);

    String driverName;
    Class<? extends WebDriver> driverClass;

    WebDriverFactory(String name, Class<? extends WebDriver> clazz) {
        driverName = name;
        driverClass = clazz;
    }

    public static WebDriver getInstance(String value) {
        if (value == null) {
            throw new IllegalArgumentException("A web driver name must be provided.");
        }

        //support for friendly names
        for (WebDriverFactory type : values()) {
            if (type.driverName.equalsIgnoreCase(value)) {
                return type.getWebDriver();
            }
        }

        return valueOf(value.toUpperCase()).getWebDriver();
    }

    WebDriver getWebDriver() {
        try {
            return driverClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Could not instantiate class for '" + driverClass.getSimpleName() + "'", e);
        }
    }
}
