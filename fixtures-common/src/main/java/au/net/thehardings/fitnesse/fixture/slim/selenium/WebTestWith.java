package au.net.thehardings.fitnesse.fixture.slim.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This class
 * <pre>
 * !|script                |web test with    |HTML Unit        |
 * |go to page             |http://localhost                   |
 * |check                  |title is         |FrontPage        |
 * |follow link with text  |Fixture Samples                    |
 * |follow link with text  |Comparator Samples +               |
 * |ensure                 |title contains   |ComparatorSamples|
 * |follow link with text  |Test                               |
 * |until title starts with|Test Results:                      |
 * |ensure                 |title starts with|Test Results:    |
 * |quit                                                       |
 * </pre>
 */
public class WebTestWith {
    public static final int DEFAULT_TIMEOUT = 10;

    int defaultTimeout;
    WebDriver driver;
    WebElement element;

    public WebTestWith(String driverName) {
        this(driverName, DEFAULT_TIMEOUT);
    }

    public WebTestWith(String driverName, int defTimeout) {
        driver = WebDriverFactory.getInstance(driverName);
        defaultTimeout = defTimeout;
    }

    public void goToPage(String url) {
        driver.get(url);
    }

    public void findElementNamed(String name) {
        element = driver.findElement(By.name(name));

    }

    public void findElementWithId(String id) {
        element = driver.findElement(By.id(id));
    }

    public void enterText(String text) {
        element.sendKeys(text);
    }

    public void andSubmit() {
        element.submit();
    }

    public void findLinkWithText(String text) {
        element = driver.findElement(By.linkText(text));
    }

    public void findLinkContainingText(String text) {
        element = driver.findElement(By.partialLinkText(text));
    }

    public void followLinkWithText(String text) {
        findLinkWithText(text);
        andClick();
    }

    public void followLinkContainingText(String text) {
        findLinkContainingText(text);
        andClick();
    }

    public void andClick() {
        element.click();
    }

    public void clickBackButton() {
        driver.navigate().back();
    }

    public void clickRefreshButton() {
        driver.navigate().refresh();
    }

    public void clickForwardButton() {
        driver.navigate().forward();
    }

    //wait methods

    public void waitUntilTitleStartsWith(String text) {
        waitUntilTitleStartsWith(defaultTimeout, text);
    }

    public void waitUntilTitleStartsWith(int secs, final String text) {
        wait(secs, new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().startsWith(text.toLowerCase());
            }
        });
    }

    public void waitUntilTitleContains(String text) {
        waitUntilTitleContains(defaultTimeout, text);
    }

    public void waitUntilTitleContains(int secs, final String text) {
        wait(secs, new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getTitle().toLowerCase().contains(text.toLowerCase());
            }
        });
    }

    public void waitUntilElementWithIdExists(String id) {
        waitUntilElementWithIdExists(defaultTimeout, id);
    }

    public void waitUntilElementWithIdExists(int secs, String id) {
        wait(secs, ExpectedConditions.presenceOfElementLocated(By.id(id)));
    }

    public void waitUntilElementWithNameExists(String name) {
        waitUntilElementWithNameExists(defaultTimeout, name);
    }

    public void waitUntilElementWithNameExists(int secs, String name) {
        wait(secs, ExpectedConditions.presenceOfElementLocated(By.name(name)));
    }

    public void waitUntilElementWithIdIsClickable(String id) {
        waitUntilElementWithIdIsClickable(defaultTimeout, id);
    }

    public void waitUntilElementWithIdIsClickable(int secs, String id) {
        wait(secs, ExpectedConditions.elementToBeClickable(By.id(id)));
    }

    public void waitUntilElementWithIdStartsWith(String id, String text) {
        waitUntilElementWithIdStartsWith(defaultTimeout, id, text);
    }

    public void waitUntilElementWithIdStartsWith(int secs, final String id, final String text) {
        wait(secs, new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.id(id)).getText().toLowerCase().startsWith(text.toLowerCase());
            }
        });
    }

    public void waitUntilElementWithIdContains(String id, String text) {
        waitUntilElementWithIdContains(defaultTimeout, id, text);
    }

    public void waitUntilElementWithIdContains(int secs, final String id, final String text) {
        wait(secs, new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.id(id)).getText().toLowerCase().contains(text.toLowerCase());
            }
        });
    }

    public void waitUntilElementWithNameStartsWith(String name, String text) {
        waitUntilElementWithNameStartsWith(defaultTimeout, name, text);
    }

    public void waitUntilElementWithNameStartsWith(int secs, final String name, final String text) {
        wait(secs, new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.name(name)).getText().toLowerCase().startsWith(text.toLowerCase());
            }
        });
    }

    public void waitUntilElementWithNameContains(String name, String text) {
        waitUntilElementWithNameContains(defaultTimeout, name, text);
    }

    public void waitUntilElementWithNameContains(int secs, final String name, final String text) {
        wait(secs, new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.name(name)).getText().toLowerCase().contains(text.toLowerCase());
            }
        });
    }

    public void waitUntilElementWithNameIsClickable(String name) {
        waitUntilElementWithNameIsClickable(defaultTimeout, name);
    }

    public void waitUntilElementWithNameIsClickable(int secs, String name) {
        wait(secs, ExpectedConditions.elementToBeClickable(By.name(name)));
    }

    public void waitUntilTagWithNameExists(String name) {
        waitUntilTagWithNameExists(defaultTimeout, name);
    }

    public void waitUntilTagWithNameExists(int secs, String name) {
        wait(secs, ExpectedConditions.presenceOfElementLocated(By.tagName(name)));
    }

    public void waitUntilLinkWithTextExists(String text) {
        waitUntilLinkWithTextExists(defaultTimeout, text);
    }

    public void waitUntilLinkWithTextExists(int secs, String text) {
        wait(secs, ExpectedConditions.presenceOfElementLocated(By.linkText(text)));
    }

    /**
     * This should be a last resort - ideally a wait method should be implemented and added to the above.
     * @param sleepTime - time to wait in seconds
     */
    public void pauseFor(int sleepTime) {
        //wait for the configured period
        try {
            Thread.sleep(sleepTime * 1000);
        } catch (InterruptedException e) {
            //best attempt
        }
    }

    private void wait(int timeout, ExpectedCondition<?> condition) {
        new WebDriverWait(driver, timeout).until(condition);
    }

    //check methods

    public String titleIs() {
        return driver.getTitle();
    }

    public String elementTextIs() {
        return element.getText();
    }

    //ensure methods

    public boolean titleStartsWith(String text) {
        return driver.getTitle().toLowerCase().startsWith(text.toLowerCase());
    }

    public boolean titleContains(String text) {
        return driver.getTitle().toLowerCase().contains(text.toLowerCase());
    }

    public boolean elementTextContains(String text) {
        return element.getText().contains(text);
    }

    public void quit() {
        driver.quit();
    }
}
