package au.net.thehardings.fitnesse.transport;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;

public class FileTransport {
    MessageFormat fileName;
    boolean overwrite = true;
    Integer version = 0;
    NumberFormat format = new DecimalFormat("000");

    public void setFileName(String fileName) {
        this.fileName = new MessageFormat(fileName);
        if (!overwrite && fileName.indexOf("{0}") == -1) {
            overwrite = true;
        }
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }

    public void sendMessage(String message) {
        try {
            File file = findFileVersion();
            FileOutputStream out = new FileOutputStream(file, false);
            out.write(message.getBytes("UTF-8"));
            out.close();
        } catch (Exception e) {
            throw new TransportException("Couldn't write the file", e);
        }
    }

    File findFileVersion() {
        Object[] versioning = new Object[]{format.format(version++)};
        File file = new File(fileName.format(versioning));
        return file.exists() && !overwrite ? findFileVersion() : file;
    }
}
