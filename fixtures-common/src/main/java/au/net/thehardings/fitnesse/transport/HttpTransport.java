package au.net.thehardings.fitnesse.transport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class should be used by Fixtures as a transport mechanism to make <b>
 * HTTP </b> <b>POST and GET</b> calls
 */
public class HttpTransport {
    private final static Log LOG = LogFactory.getLog(HttpTransport.class);

    /**
     * Allows the caller to make a http post request to the given targetUrl with
     * the given urlParameters
     *
     * @param targetURL - The URL to hit
     * @param urlParameters - The parameters to use
     * @return Returns the response of the http post request
     */
    public String executePost(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        try {
            // Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            // Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
            // Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Exception occurred while calling post... Endpoint '" + targetURL + "' with parameters " + urlParameters, ex);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
