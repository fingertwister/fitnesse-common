package au.net.thehardings.fitnesse.transport;

import au.net.thehardings.fitnesse.env.DatabaseEnvironment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Run the supplied SQL message against a database.
 */
public class JdbcTransport {
    private final static Log LOG = LogFactory.getLog(JdbcTransport.class);

    DatabaseEnvironment env;
    JdbcTemplate template;

    public void setEnv(String environment) {
        this.env = DatabaseEnvironment.factory(environment);
        template = new JdbcTemplate(env.getDataSource());
    }

    public void sendMessage(String message) {
        LOG.info("Running Query [" + message + "]");
        template.update(message);
    }

    public Object queryForObject(String sql) {
        return template.queryForObject(sql, Object.class);
    }
}
