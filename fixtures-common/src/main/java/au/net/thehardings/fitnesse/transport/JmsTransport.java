package au.net.thehardings.fitnesse.transport;

import au.net.thehardings.fitnesse.env.QueueEnvironment;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.io.UnsupportedEncodingException;

public class JmsTransport {
    QueueEnvironment env;
    String queue;
    long pauseMillis = 0L;

    public void setEnv(String env) {
        this.env = QueueEnvironment.factory(env);
    }

    public void setQueue(String queue) {
        this.queue = env.processQueueName(queue);
    }

    public void setPauseTime(long pauseTime) {
        this.pauseMillis = pauseTime;
    }

    public void sendMessage(String message) {
        QueueConnection qc = null;
        try {
            qc = getQueueConnection();
            QueueSession qs = qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue q = qs.createQueue(queue);
            QueueSender sender = qs.createSender(q);
            Message msg = qs.createTextMessage(message);
            sender.send(msg);
        } catch (JMSException je) {
            throw new TransportException("Exception during publish message to queue: " + queue, je);
        } finally {
            try {
                if (qc != null) qc.close();
            } catch (JMSException e) {
                //ignore
            }
        }
    }

    public void sendBytesMessage(String content, String charsetName) {
        try {
            sendMessage(content.getBytes(charsetName));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void sendMessage(byte[] byteMessageArray) {
        QueueConnection qc = null;
        try {
            qc = getQueueConnection();
            QueueSession qs = qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue q = qs.createQueue(queue);
            QueueSender sender = qs.createSender(q);
            BytesMessage byteMsg = qs.createBytesMessage();
            byteMsg.writeBytes(byteMessageArray);
            sender.send(byteMsg);
        } catch (JMSException je) {
            throw new TransportException("Exception during publish message to queue: " + queue, je);
        } finally {
            try {
                if (qc != null) qc.close();
            } catch (JMSException e) {
                //ignore
            }
        }
    }

    /**
     * Receive message.
     *
     * @return The message text from the queue
     */
    public String receiveMessage() {
        String messageConsumed = "";
        QueueConnection qc = null;
        try {
            qc = getQueueConnection();
            //start the transaction
            qc.start();
            QueueSession qs = qc.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);
            Queue q = qs.createQueue(queue);
            MessageConsumer consumer = qs.createConsumer(q);
            Message msg = consumer.receive(30000);
            if (msg instanceof TextMessage) {
                TextMessage txtMsg = (TextMessage) msg;
                messageConsumed = txtMsg.getText();
            } else if (msg instanceof BytesMessage) {
                BytesMessage byteMsg = (BytesMessage) msg;
                byte[] byteMessageArray = new byte[(int) byteMsg.getBodyLength()];
                byteMsg.readBytes(byteMessageArray);
                messageConsumed = new String(byteMessageArray, "UTF-8");
            }
            //commit the transaction
            qs.commit();
            return messageConsumed;
        } catch (JMSException je) {
            throw new TransportException("Exception during consume message to queue: " + queue, je);
        } catch (UnsupportedEncodingException uee) {
            throw new TransportException("Exception while encoding UTF-8: ", uee);
        } finally {
            try {
                if (qc != null) qc.close();
            } catch (JMSException e) {
                //ignore
            }
        }
    }

    /**
     * Receive message.
     *
     * @return number of messages cleared from the queue
     */
    public int clearAllMessages() {
        int numMessages = 0;
        QueueConnection qc = null;
        try {
            qc = getQueueConnection();
            //start the transaction
            qc.start();
            QueueSession qs = qc.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);
            Queue q = qs.createQueue(queue);
            QueueReceiver receiver = qs.createReceiver(q);
            //noted that some JMS clients (ActiveMQ) run some processes async and messages can be missed
            //by receiveNoWait(). The pause period needs to be injected and defaults to 0 for minimal impact
            pause();
            while (receiver.receiveNoWait() != null) {
                numMessages++;
            }
            qs.commit();
            return numMessages;

        } catch (JMSException je) {
            throw new TransportException("Exception during clearing messages from queue: " + queue, je);
        } finally {
            try {
                if (qc != null) qc.close();
            } catch (JMSException e) {
                //ignore
            }
        }
    }

    /**
     * Receive message.
     *
     * @return number of messages on the queue
     */
    public int getQueueDepth() {
        int numMessages = 0;
        QueueConnection qc = null;
        try {
            qc = getQueueConnection();
            qc.start();
            QueueSession qs = qc.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);
            Queue q = qs.createQueue(queue);
            //ideally this would use a queue browser, but some JMS implementations wait the timeout period on the
            //hasMoreElements which results in a significant pause when running tests. This implementation while
            //a little bit clunky with a transaction rollback manages to perform much better.
            QueueReceiver receiver = qs.createReceiver(q);
            //noted that some JMS implementations (ActiveMQ) run some processes async and messages can be missed
            //by receiveNoWait(). The pause period needs to be injected and defaults to 0 for minimal impact.
            pause();
            //QueueReceiver.receiveNoWait() is much faster than QueueBrowser.getEnumeration().hasMoreElements()
            while (receiver.receiveNoWait() != null) {
                numMessages++;
            }
            //need to rollback so messages are not lost.
            qs.rollback();
            return numMessages;

        } catch (JMSException je) {
            throw new TransportException("Exception during count messages on queue: " + queue, je);
        } finally {
            try {
                if (qc != null) qc.close();
            } catch (JMSException e) {
                //ignore
            }
        }

    }

    protected QueueConnection getQueueConnection() throws JMSException {
        QueueConnectionFactory cf = env.getConnectionFactory();
        return cf.createQueueConnection(env.getUsername(), env.getPassword());
    }

    void pause() {
        try {
            Thread.sleep(pauseMillis);
        } catch (InterruptedException e) {
            //do nothing
        }
    }


}