package au.net.thehardings.fitnesse.transport;

public class TransportException extends RuntimeException {
    public TransportException(String message, Throwable cause) {
        super(message, cause);
    }
}
