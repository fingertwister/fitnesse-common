package au.net.thehardings.fitnesse.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class DefaultEncryptionUtilImpl implements EncryptionUtil {
    static final String DES = "DESede";
    static final String UNICODE_FORMAT = "UTF-8";
    static final String DEFAULT_KEY = "asdl;d09ApD0nmX#Og=82-1[";

    public static void main(String[] args) {
        EncryptionUtil util = new DefaultEncryptionUtilImpl();
        System.out.print(String.format("Encrypting for user '%s' with password '%s': ", args[0], args[1]));
        System.out.println(util.encrypt(args[0], args[1]));
    }

    public String encrypt(String encryptionKey, String unencryptedString) {
        if (unencryptedString == null || unencryptedString.length() == 0) {
            //empty string so nothing to do
            return "";
        }

        try {
            Cipher cipher = getCipher(encryptionKey, 1);
            byte[] cleartext = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] ciphertext = cipher.doFinal(cleartext);
            Base64 base64encoder = new Base64();
            return base64encoder.encodeToString(ciphertext).trim();
        }
        catch (Exception e) {
            return unencryptedString;
        }
    }

    public String decrypt(String encryptionKey, String encryptedString) {
        if (encryptedString == null || encryptedString.length() <= 0) {
            //empty string so nothing to do
            return "";
        }

        try {
            Cipher cipher = getCipher(encryptionKey, 2);
            Base64 base64decoder = new Base64();
            byte[] ciphertext = base64decoder.decode(encryptedString);
            byte[] cleartext = cipher.doFinal(ciphertext);

            return bytes2String(cleartext);
        }
        catch (Exception e) {
            return encryptedString;
        }
    }

    Cipher getCipher(String encryptionKey, int mode) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException {
        byte[] keyAsBytes = getPassphrase(encryptionKey).getBytes(UNICODE_FORMAT);
        KeySpec keySpec = new DESedeKeySpec(keyAsBytes);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        Cipher cipher = Cipher.getInstance(DES);
        SecretKey key = keyFactory.generateSecret(keySpec);
        cipher.init(mode, key);
        return cipher;
    }

    String bytes2String(byte[] bytes) throws IOException {
        StringWriter writer = new StringWriter();
        InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(bytes), "UTF8");
        char[] chars = new char[bytes.length];
        reader.read(chars);
        writer.write(chars);
        return writer.toString();
    }

    String getPassphrase(String seed) {
        String salt = seed == null ? "" : seed;
        if (salt.length() < 24) {
            return salt + DEFAULT_KEY.substring(salt.length());
        } else {
            return salt;
        }
    }
}
