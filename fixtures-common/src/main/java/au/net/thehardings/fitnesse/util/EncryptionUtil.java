package au.net.thehardings.fitnesse.util;

public interface EncryptionUtil {
    String encrypt(String encryptionKey, String unencryptedString);

    String decrypt(String encryptionKey, String encryptedString);
}
