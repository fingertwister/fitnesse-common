package au.net.thehardings.fitnesse.util;

import fit.Fixture;

public class StringUtil {

    public String replaceSymbols(String string) {
        String symbolString = string;
        while (symbolString.indexOf("$[") > 0) {
            int qualifierStart = symbolString.indexOf("$[") + 2;
            int qualifierEnd = symbolString.indexOf(']');
            String qualifier = symbolString.substring(qualifierStart, qualifierEnd);
            Object value = Fixture.getSymbol(qualifier);
            if (value == null || "null".equals(value)) {
                throw new IllegalArgumentException("Could not find an entry for $[" + qualifier + "] in the symbol list. Query is:\n" + symbolString);
            }
            symbolString = symbolString.replaceAll("\\$\\[" + qualifier + "\\]", value.toString());
        }
        return symbolString;
    }

    public String stripHtmlLineBreaks(String string) {
        return string.replaceAll("<br/>", "").replaceAll("<BR/>", "");
    }
}
