package au.net.thehardings.fitnesse.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * This class provides an easy mechanism for date arithmetic to be performed in
 * the template.
 */
public class TemplateCalendar extends GregorianCalendar {
    public Calendar add(int days) {
        Calendar calendar = new TemplateCalendar();
        calendar.setTimeInMillis(getTimeInMillis());
        calendar.add(Calendar.DATE, days);
        return calendar;
    }
}
