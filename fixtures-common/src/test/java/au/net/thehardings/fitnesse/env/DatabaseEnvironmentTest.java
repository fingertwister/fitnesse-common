package au.net.thehardings.fitnesse.env;

import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;

/**
 * A JUnit test class for QueueEnvironment
 */
public class DatabaseEnvironmentTest {

    @Test(expected = IllegalArgumentException.class)
    public void testFactory() throws Exception {
        Assert.assertNotNull("Should be null-safe", DatabaseEnvironment.factory("DEV"));
        DatabaseEnvironment.factory("DOES_NOT_EXIST"); //throws exception
    }

    @Test
    public void testGetDataSource() throws Exception {
        Connection connection = DatabaseEnvironment.factory("DEV").getDataSource().getConnection();
        connection.close();
    }
}
