package au.net.thehardings.fitnesse.env;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class EnvBeanFactoryTest {

    @Test
    public void testGetInstance() throws Exception {
        assertNotNull("An env bean factory should have been constructed", EnvBeanFactory.getInstance());
    }
}