package au.net.thehardings.fitnesse.env;

import org.junit.Assert;
import org.junit.Test;

/**
 * A JUnit test class for QueueEnvironment
 */
public class QueueEnvironmentTest {

    @Test
    public void testProcessQueueName() throws Exception {
        Assert.assertNull("Should be null-safe", QueueEnvironment.factory("DEV").processQueueName(null));
        Assert.assertNull("Should be illegal argument safe", QueueEnvironment.factory("DEV").processQueueName(""));
        Assert.assertEquals("Drop should be added on - scenario 1", "queue.drop1", QueueEnvironment.factory("DEV.DROP").processQueueName("queue"));
        Assert.assertEquals("Drop should be added on - scenario 2", "queue.name.drop1", QueueEnvironment.factory("DEV.DROP").processQueueName("queue.name"));
        Assert.assertEquals("Drop should not be added if already there - scenario 1", "queue.drop3", QueueEnvironment.factory("DEV.DROP").processQueueName("queue.drop3"));
        Assert.assertEquals("Drop should not be added if already there - scenario 2", "QUEUE.DROP3", QueueEnvironment.factory("DEV.DROP").processQueueName("QUEUE.DROP3"));
        Assert.assertEquals("Empty drops should be supported - scenario 1", "queue", QueueEnvironment.factory("DEV").processQueueName("queue"));
        Assert.assertEquals("Empty drops should be supported - scenario 2", "queue.name", QueueEnvironment.factory("DEV").processQueueName("queue.name"));
    }

    @Test
    public void testGetConnectionFactory() throws Exception {
        QueueEnvironment subject = QueueEnvironment.factory("DEV");
        subject.getConnectionFactory();
    }
}
