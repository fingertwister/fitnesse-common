package au.net.thehardings.fitnesse.fixture;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AddSymbolTest {
    public static final String VALUE = "value";
    AddSymbol subject;

    @Before
    public void setUp() {
        subject = new AddSymbol();
    }

    @Test
    public void testSetSymbol() throws Exception {
        subject.setSymbolValue(VALUE);
        assertEquals("Should just have returned the value", VALUE, subject.setSymbol());
    }
}