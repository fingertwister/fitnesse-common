package au.net.thehardings.fitnesse.fixture;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class CheckLogFileTest {
    CheckLogFile subject;

    @Before
    public void setUp() {
        subject = new CheckLogFile();
    }

    @After
    public void tearDown() {
        subject = null;
    }

    @Test
    public void testCheckLogFile() throws Exception {
        subject = new CheckLogFile("logFileName", "logDateFormat", "10");
        assertEquals("logFileName not set properly", "logFileName", subject.logFileName);
        assertEquals("logDateFormat not set properly", "logDateFormat", subject.logDateFormat);
        assertEquals("lookBackSeconds not set properly", 10, 0, subject.lookBackSeconds);
    }

    @Test
    public void testCheckLogFileWithDefault() throws Exception {
        subject = new CheckLogFile("logFileName", "logDateFormat");
        assertEquals("logFileName not set properly", "logFileName", subject.logFileName);
        assertEquals("logDateFormat not set properly", "logDateFormat", subject.logDateFormat);
        assertEquals("lookBackSeconds not set to default", 30, 0, subject.lookBackSeconds);
    }

    @Test
    public void testInitialiseFit() throws Exception {
        subject = new TestableCheckLogFile(toArray("logFileName", "logDateFormat", "10"));
        subject.initialiseFit();
        assertEquals("logFileName not set properly", "logFileName", subject.logFileName);
        assertEquals("logDateFormat not set properly", "logDateFormat", subject.logDateFormat);
        assertEquals("lookBackSeconds not set properly", 10, 0, subject.lookBackSeconds);
    }

    @Test
    public void testInitialiseFitWithDefault() throws Exception {
        subject = new TestableCheckLogFile(toArray("logFileName", "logDateFormat"));
        subject.initialiseFit();
        assertEquals("logFileName not set properly", "logFileName", subject.logFileName);
        assertEquals("logDateFormat not set properly", "logDateFormat", subject.logDateFormat);
        assertEquals("lookBackSeconds not set to default", 30, 0, subject.lookBackSeconds);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException1() throws Exception {
        subject = new TestableCheckLogFile(new String[]{});
        subject.initialiseFit();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException2() throws Exception {
        subject = new TestableCheckLogFile(toArray("logFileName"));
        subject.initialiseFit();
    }

    @Test
    public void testCheckLog() throws Exception {
        //construct the scanner
        String logDateFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat dateFormatter = new SimpleDateFormat(logDateFormat);
        Calendar calender = Calendar.getInstance();
        StringWriter writer = new StringWriter();
        writer.write("Header Message\n");
        writer.write("Non-conformant log entry with no log date that should be ignored\n");
        writer.write(dateFormatter.format(calender.getTime()));
        writer.write(",901 INFO  [org.jboss.jms.server.connectionfactory.ConnectionFactory] (JBoss Shutdown Hook) org.jboss.jms.server.connectionfactory.ConnectionFactory@16fae81 undeployed\n");
        writer.write(dateFormatter.format(calender.getTime()) + ",902 ");
        writer.write("INFO  [org.jboss.web.tomcat.service.deployers.TomcatDeployment] (JBoss Shutdown Hook) undeploy, ctxPath=/web-console\n");
        Scanner scanner = new Scanner(new ByteArrayInputStream(writer.toString().getBytes("UTF-8")));

        //JUnit testcase to test the parser to check "ctxPath=/web-console"
        subject.lookBackSeconds = 2;
        subject.message = "ctxPath=/web-console";
        subject.logDateFormat = logDateFormat;
        boolean result = subject.checkLog(scanner);
        assertEquals(true, result);
    }

    @Test
    public void testInLog() throws Exception {
        String currentPath = new File(".").getAbsolutePath();
        currentPath = currentPath.substring(0, currentPath.indexOf("fitnesse-common"));
        subject.logFileName = currentPath + "fitnesse-common/fixtures-common/src/test/resources/environment-ctx.xml";
        subject.logDateFormat = "yyyy-MM-dd HH:mm:ss";
        subject.setMessage("Some <invalid< XML looks like this & that");
        assertFalse("The sample message should not have been found", subject.inLog());
    }

    @Test(expected = FileNotFoundException.class)
    public void testInLogNoFile() throws Exception {
        subject.logFileName = "anInvalidFile.log";
        subject.inLog();
    }

    String[] toArray(String... array) {
        return array;
    }

    public class TestableCheckLogFile extends CheckLogFile {
        public TestableCheckLogFile(String[] fitArgs) {
            args = fitArgs;
        }
    }
}
