package au.net.thehardings.fitnesse.fixture;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.Scanner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CheckRemoteLogFileTest {
    CheckRemoteLogFile subject;

    @Mock
    JSch j;
    @Mock
    Session s;
    @Mock
    ChannelExec c;
    @Mock
    InputStream i;
    @Mock
    OutputStream o;

    @Before
    public void setUp() throws Exception {
        subject = new CheckRemoteLogFile();
        when(s.openChannel("exec")).thenReturn(c);
        when(c.getInputStream()).thenReturn(i);
        when(c.getOutputStream()).thenReturn(o);
    }

    @Test
    public void testCheckRemoteLogFile() throws Exception {
        subject = new CheckRemoteLogFile("user/pass@machine:/path/to/file.log", "log date format", "90000", "charset");
        assertEquals("User name was not set correctly", "user", subject.user);
        assertEquals("Password was not set correctly", "pass", subject.password);
        assertEquals("Host not set correctly", "machine", subject.host);
        assertEquals("Look-back seconds not set correctly", 90000, subject.lookBackSeconds);
        assertEquals("Charset not set correctly", "charset", subject.charset);
        assertEquals("log file not set correctly", "/path/to/file.log", subject.logFileName);
        assertEquals("log date format not set correctly", "log date format", subject.logDateFormat);
    }

    @Test
    public void testCheckRemoteLogFileWithDefaultCharset() throws Exception {
        subject = new CheckRemoteLogFile("user/pass@machine:/path/to/file.log", "log date format", "90000");
        assertEquals("User name was not set correctly", "user", subject.user);
        assertEquals("Password was not set correctly", "pass", subject.password);
        assertEquals("Host not set correctly", "machine", subject.host);
        assertEquals("Look-back seconds not set correctly", 90000, subject.lookBackSeconds);
        assertEquals("Charset not set to default", CheckRemoteLogFile.DEFAULT_CHARSET, subject.charset);
        assertEquals("log file not set correctly", "/path/to/file.log", subject.logFileName);
        assertEquals("log date format not set correctly", "log date format", subject.logDateFormat);
    }

    @Test
    public void testCheckRemoteLogFileWithDefaultLookback() throws Exception {
        subject = new CheckRemoteLogFile("user/pass@machine:/path/to/file.log", "log date format");
        assertEquals("User name was not set correctly", "user", subject.user);
        assertEquals("Password was not set correctly", "pass", subject.password);
        assertEquals("Host not set correctly", "machine", subject.host);
        assertEquals("Look-back seconds not set to default", 30L, subject.lookBackSeconds);
        assertEquals("Charset not set to default", CheckRemoteLogFile.DEFAULT_CHARSET, subject.charset);
        assertEquals("log file not set correctly", "/path/to/file.log", subject.logFileName);
        assertEquals("log date format not set correctly", "log date format", subject.logDateFormat);
    }

    @Test
    public void testCheckRemoteLogFileWithNoPass() throws Exception {
        subject = new CheckRemoteLogFile("user@machine:/path/to/file.log", "log date format");
        assertEquals("User name was not set correctly", "user", subject.user);
        assertEquals("Password should default to username", "user", subject.password);
    }

    @Test
    public void testCheckRemoteLogFileWithEncryptedPass() throws Exception {
        subject = new CheckRemoteLogFile("user/YsScCtlVK2o=@machine:/path/to/file.log", "log date format");
        assertEquals("User name was not set correctly", "user", subject.user);
        assertEquals("Password should be decrypted", "pass", subject.password);
    }

    @Test
    public void testFileExists() throws Exception {
        //setup
        TestableCheckRemoteLogFile subject = new TestableCheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        //we should verify the buffer has only 5 bytes in it.
        doAnswer(new Answer<byte[]>() {
            public byte[] answer(InvocationOnMock invocation) throws Throwable {
                byte[] buffer = (byte[]) invocation.getArguments()[0];
                assertTrue("the buffer should be 5 bytes long", buffer.length == 5);
                return null;
            }
        }).when(i).read(Matchers.<byte[]>anyObject());

        //run the test
        assertTrue("Expected the file to exist", subject.fileExists());

        //verify
        verify(s).connect();
        verify(c).setCommand("scp -f /path/to/file.log");
        verify(c).connect();
        assertTrue("checkAck() should have been called", subject.checkAckCalled);
        verify(i).read(Matchers.<byte[]>anyObject());
        assertTrue("getFileSize() should have been called", subject.getFileSizeCalled);
        verify(s).disconnect();
    }

    @Test
    public void testFileExistsReturnsFalse1() throws Exception {
        //setup
        TestableCheckRemoteLogFile subject = new TestableCheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        subject.checkAckResponse = false;

        //run the test
        assertFalse("Expected the file to not exist", subject.fileExists());

        //verify
        verify(s).connect();
        verify(c).setCommand("scp -f /path/to/file.log");
        verify(c).connect();
        assertTrue("checkAck() should have been called", subject.checkAckCalled);
        verify(s).disconnect();
    }

    @Test
    public void testFileExistsReturnsFalse2() throws Exception {
        //setup
        TestableCheckRemoteLogFile subject = new TestableCheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        subject.e = new RuntimeException("No such file or directory");

        //run the test
        assertFalse("Expected the file to not exist", subject.fileExists());

        //verify
        verify(s).connect();
        verify(c).setCommand("scp -f /path/to/file.log");
        verify(c).connect();
        assertTrue("checkAck() should have been called", subject.checkAckCalled);
        verify(s).disconnect();
    }

    @Test(expected = RuntimeException.class)
    public void testFileExistsThrowsException() throws Exception {
        //setup
        TestableCheckRemoteLogFile subject = new TestableCheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        subject.e = new RuntimeException();

        //run the test
        try {
            subject.fileExists();
        } finally {
            //verify
            verify(s).connect();
            verify(c).setCommand("scp -f /path/to/file.log");
            verify(c).connect();
            assertTrue("checkAck() should have been called", subject.checkAckCalled);
            verify(s).disconnect();
        }
    }

    @Test
    public void testInLog() throws Exception {
        //setup
        TestableCheckRemoteLogFile subject = new TestableCheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        //we should verify the buffer has only 5 bytes in it.
        doAnswer(new Answer<byte[]>() {
            public byte[] answer(InvocationOnMock invocation) throws Throwable {
                byte[] buffer = (byte[]) invocation.getArguments()[0];
                assertTrue("the buffer should be 5 bytes long", buffer.length == 5);
                return null;
            }
        }).when(i).read(Matchers.<byte[]>anyObject());

        //run the test
        assertTrue("Expected the file to exist", subject.inLog());

        //verify
        verify(s).connect();
        verify(c).setCommand("scp -f /path/to/file.log");
        verify(c).connect();
        assertTrue("checkAck() should have been called", subject.checkAckCalled);
        verify(i).read(Matchers.<byte[]>anyObject());
        assertTrue("getFileSize() should have been called", subject.getFileSizeCalled);
        assertTrue("getFileName() should have been called", subject.getFileNameCalled);
        assertTrue("checkLog(scanner, fileSize) should have been called", subject.checkLogCalled);
        assertEquals("fileSize to checkLog(scanner, fileSize) should be fileSize from getFileSize()", subject.fileSize, subject.fileSizeInParam);
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
        verify(s).disconnect();
    }

    @Test
    public void testInLogReturnsFalse1() throws Exception {
        //setup
        TestableCheckRemoteLogFile subject = new TestableCheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        subject.checkAckResponse = false;

        //run the test
        assertFalse("Expected the file to not exist", subject.inLog());

        //verify
        verify(s).connect();
        verify(c).setCommand("scp -f /path/to/file.log");
        verify(c).connect();
        assertTrue("checkAck() should have been called", subject.checkAckCalled);
        verify(s).disconnect();
    }

    @Test
    public void testInLogReturnsFalse2() throws Exception {
        //setup
        TestableCheckRemoteLogFile subject = new TestableCheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        subject.checkLogResponse = false;
        //we should verify the buffer has only 5 bytes in it.
        doAnswer(new Answer<byte[]>() {
            public byte[] answer(InvocationOnMock invocation) throws Throwable {
                byte[] buffer = (byte[]) invocation.getArguments()[0];
                assertTrue("the buffer should be 5 bytes long", buffer.length == 5);
                return null;
            }
        }).when(i).read(Matchers.<byte[]>anyObject());

        //run the test
        assertFalse("Expected the log to not find the message", subject.inLog());

        //verify
        verify(s).connect();
        verify(c).setCommand("scp -f /path/to/file.log");
        verify(c).connect();
        assertTrue("checkAck() should have been called", subject.checkAckCalled);
        verify(i).read(Matchers.<byte[]>anyObject());
        assertTrue("getFileSize() should have been called", subject.getFileSizeCalled);
        assertTrue("getFileName() should have been called", subject.getFileNameCalled);
        assertTrue("checkLog(scanner, fileSize) should have been called", subject.checkLogCalled);
        assertEquals("fileSize to checkLog(scanner, fileSize) should be fileSize from getFileSize()", subject.fileSize, subject.fileSizeInParam);
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
        verify(s).disconnect();
    }

    @Test
    public void testCheckLog() throws Exception {
        //construct the scanner
        String logDateFormat = "yyyy-MM-dd HH:mm:ss";
        DateFormat dateFormatter = new SimpleDateFormat(logDateFormat);
        Calendar calender = Calendar.getInstance();
        StringWriter writer = new StringWriter();
        writer.write("Header Message\n");
        writer.write("Non-conformant log entry with no log date that should be ignored\n");
        writer.write(dateFormatter.format(calender.getTime()));
        writer.write(",901 INFO  [org.jboss.jms.server.connectionfactory.ConnectionFactory] (JBoss Shutdown Hook) org.jboss.jms.server.connectionfactory.ConnectionFactory@16fae81 undeployed\n");
        writer.write(dateFormatter.format(calender.getTime()) + ",902 ");
        writer.write("INFO  [org.jboss.web.tomcat.service.deployers.TomcatDeployment] (JBoss Shutdown Hook) undeploy, ctxPath=/web-console\n");
        Scanner scanner = new Scanner(new ByteArrayInputStream(writer.toString().getBytes("UTF-8")));

        //JUnit testcase to test the parser to check "ctxPath=/web-console"
        subject.lookBackSeconds = 2;
        subject.message = "ctxPath=/web-console";
        subject.logDateFormat = logDateFormat;

        //run the test (part 1)
        assertFalse("Did not honour fileSize", subject.checkLog(scanner, 267L));

        //reset
        scanner = new Scanner(new ByteArrayInputStream(writer.toString().getBytes("UTF-8")));

        //run the test (part 2)
        assertTrue(subject.checkLog(scanner, 408L));
    }

    @Test
    public void testGetSession() throws Exception {
        //setup
        subject = new CheckRemoteLogFile("user/pass@machine:/path/to/file.log", "yyyy-MM-dd HH:mm:ss");
        subject.jsch = j;
        when(j.getSession(subject.user, subject.host, 22)).thenReturn(s);
        //Add a setup check that strict host checking has been disabled
        doAnswer(new Answer<Properties>() {
            public Properties answer(InvocationOnMock invocation) throws Throwable {
                Properties config = (Properties) invocation.getArguments()[0];
                assertEquals("Strict Host Checking should be disabled", "no", config.get("StrictHostKeyChecking"));
                return null;
            }
        }).when(s).setConfig(any(Properties.class));

        //run the test
        assertSame("Should have got the mock session back", s, subject.getSession());

        //verify
        verify(j).setKnownHosts(CheckRemoteLogFile.KNOWN_HOSTS);
        verify(s).setPassword(subject.password);
        verify(s).setConfig(any(Properties.class));
    }

    @Test
    public void testCheckAck() throws Exception {
        //setup
        InputStream in = new ByteArrayInputStream("C".getBytes());

        //run the test
        assertTrue("Expected the ack to work", subject.checkAck(in, o));

        //verify
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
    }

    @Test
    public void testCheckAckFalse() throws Exception {
        //setup
        when(i.read()).thenReturn(-1);

        //run the test
        assertFalse("Expected the ack to fail", subject.checkAck(i, o));

        //verify
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
    }

    @Test
    public void testCheckAckException1() throws Exception {
        //setup
        InputStream in = new ByteArrayInputStream(((char) 1 + "Bummer\n").getBytes());

        //run the test. We do this the 'old-school' way to check the exception message
        try {
            assertFalse("Expected the ack to fail", subject.checkAck(in, o));
            fail("An exception should have been thrown");
        } catch (RuntimeException e) {
            assertEquals("The message should have been constructed from the InputStream", "Bummer", e.getMessage());
        }

        //verify
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
    }

    @Test
    public void testCheckAckException2() throws Exception {
        //setup
        InputStream in = new ByteArrayInputStream(((char) 2 + "Bummer\n").getBytes());

        //run the test. We do this the 'old-school' way to check the exception message
        try {
            assertFalse("Expected the ack to fail", subject.checkAck(in, o));
            fail("An exception should have been thrown");
        } catch (RuntimeException e) {
            assertEquals("The message should have been constructed from the InputStream", "Bummer", e.getMessage());
        }

        //verify
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
    }

    @Test
    public void testGetFileSize() throws Exception {
        //setup
        InputStream in = new ByteArrayInputStream("95634 ".getBytes());

        //run the test
        assertEquals("The file size was not correct", 95634L, subject.getFileSize(in, o));

        //verify
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
    }

    @Test(expected = RuntimeException.class)
    public void testGetFileSizeException() throws Exception {
        //setup
        when(i.read((byte[]) anyObject())).thenReturn(-1);

        //run the test
        subject.getFileSize(i, o);
    }

    @Test
    public void testGetFileName() throws Exception {
        //setup
        InputStream in = new ByteArrayInputStream(("filename.log" + (char) CheckRemoteLogFile.NULL).getBytes());

        //run the test.
        assertEquals("File name was not read successfully", "filename.log", subject.getFileName(in, o));

        //verify
        verify(o).write(CheckRemoteLogFile.HANDSHAKE);
        verify(o).flush();
    }


    public class TestableCheckRemoteLogFile extends CheckRemoteLogFile {
        public RuntimeException e = null;
        public boolean checkAckCalled = false;
        public boolean checkAckResponse = true;
        public boolean getFileSizeCalled = false;
        public boolean getFileNameCalled = false;
        public boolean checkLogCalled = false;
        public boolean checkLogResponse = true;
        public long fileSize = 30L;
        public long fileSizeInParam = 0L;

        public TestableCheckRemoteLogFile() {
        }

        public TestableCheckRemoteLogFile(String logFileName, String logDateFormat) {
            super(logFileName, logDateFormat);
        }

        public TestableCheckRemoteLogFile(String logFileName, String logDateFormat, String lookBackSeconds) {
            super(logFileName, logDateFormat, lookBackSeconds);
        }

        public TestableCheckRemoteLogFile(String logFileName, String logDateFormat, String lookBackSeconds, String characterSet) {
            super(logFileName, logDateFormat, lookBackSeconds, characterSet);
        }

        @Override
        Session getSession() throws JSchException {
            return s;
        }

        @Override
        boolean checkAck(InputStream in, OutputStream out) throws IOException {
            checkAckCalled = true;
            if (e != null) throw e;
            return checkAckResponse;
        }

        @Override
        long getFileSize(InputStream in, OutputStream out) throws IOException {
            getFileSizeCalled = true;
            return fileSize;
        }

        @Override
        String getFileName(InputStream in, OutputStream out) throws IOException {
            getFileNameCalled = true;
            return logFileName;
        }

        @Override
        protected boolean checkLog(Scanner s, long fileSize) {
            checkLogCalled = true;
            fileSizeInParam = fileSize;
            return checkLogResponse;
        }
    }
}