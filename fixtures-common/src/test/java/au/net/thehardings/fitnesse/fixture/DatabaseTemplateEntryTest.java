package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.transport.JdbcTransport;
import fit.Fixture;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseTemplateEntryTest {
    static final String MESSAGE = "A simple test message";
    DatabaseTemplateEntry subject;
    @Mock
    JdbcTransport t;
    @Mock
    Configuration c;

    @Test
    public void testSend() throws Exception {
        subject = new TestableDatabaseTemplateEntry();
        subject.transport = t;
        subject.send(MESSAGE);
        verify(t).sendMessage(MESSAGE);
    }

    @Test
    public void testDatabaseTemplateEntry() throws Exception {
        //we want a real freemarker configuration for this test, just not pointing to fitnesse files.
        c = getDummyConfiguration();
        //SLiM will replace symbols, but leave HTML line breaks in the parameter
        subject = new TestableDatabaseTemplateEntry("DEV", "some html text<br/> that spans<BR/> multiple lines.");
        verify(t).setEnv("DEV");
        StringWriter text = new StringWriter();
        subject.template.process(new HashMap(), text);
        assertEquals("some html text that spans multiple lines.", text.toString());
    }

    @Test
    public void testInitialiseFit() throws Exception {
        //we want a real freemarker configuration for this test, just not pointing to fitnesse files.
        c = getDummyConfiguration();
        //FIT will remove HTML line breaks, but not replace symbols in the parameter
        subject = new TestableDatabaseTemplateEntry(toArray("DEV", "some html text $[middleLine] multiple lines."));
        Fixture.setSymbol("middleLine", "that spans");
        subject.initialiseFit();
        Fixture.ClearSymbols();

        verify(t).setEnv("DEV");
        StringWriter text = new StringWriter();
        subject.template.process(new HashMap(), text);
        assertEquals("some html text that spans multiple lines.", text.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException1() throws Exception {
        subject = new TestableDatabaseTemplateEntry(toArray(null, "some text"));
        try {
            subject.initialiseFit();
        } finally {
            verify(t, never()).setEnv(anyString());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException2() throws Exception {
        subject = new TestableDatabaseTemplateEntry(toArray("DEV", null));
        subject.transport = t;
        try {
            subject.initialiseFit();
        } finally {
            verify(t, never()).setEnv(anyString());
        }
    }

    Configuration getDummyConfiguration() {
        Configuration config = new Configuration();
        try {
            config.setDirectoryForTemplateLoading(new File("."));
        } catch (IOException e) {
            throw new RuntimeException();
        }
        config.setObjectWrapper(new DefaultObjectWrapper());
        config.setDefaultEncoding("UTF-8");
        config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        config.setIncompatibleImprovements(new Version(2, 3, 20));
        return config;
    }

    String[] toArray(String... array) {
        return array;
    }

    public class TestableDatabaseTemplateEntry extends DatabaseTemplateEntry {

        public TestableDatabaseTemplateEntry() {
            super();
        }

        public TestableDatabaseTemplateEntry(String env, String sql) {
            super(env, sql);
        }

        public TestableDatabaseTemplateEntry(String[] fitArgs) {
            args = fitArgs;
        }

        @Override
        void configureFreemarker() {
            cfg = c;
        }

        @Override
        void configureParameters(String env, String sql) {
            //we inject the mock transport here, because there is nowhere else to do it in the constructor call chain
            super.transport = t;
            super.configureParameters(env, sql);
        }
    }
}