package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.transport.JdbcTransport;
import fit.Fixture;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseValueSelectTest {
    DatabaseValueSelect subject;
    @Mock
    JdbcTransport t;

    @Before
    public void setUp() {
        subject = new DatabaseValueSelect();
        subject.transport = t;
    }

    @Test
    public void testDatabaseTemplateEntry() throws Exception {
        //SLiM will replace symbols, but leave HTML line breaks in the parameter
        subject = new TestableDatabaseValueSelect("DEV", "some html text<br/> that spans<BR/> multiple lines.");
        verify(t).setEnv("DEV");
        assertEquals("some html text that spans multiple lines.", subject.query);
    }

    @Test
    public void testInitialiseFit() throws Exception {
        //FIT will remove HTML line breaks, but not replace symbols in the parameter
        subject = new TestableDatabaseValueSelect(toArray("DEV", "some html text $[middleLine] multiple lines."));
        Fixture.setSymbol("middleLine", "that spans");
        subject.initialiseFit();
        Fixture.ClearSymbols();

        verify(t).setEnv("DEV");
        assertEquals("some html text that spans multiple lines.", subject.query);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException1() throws Exception {
        subject = new TestableDatabaseValueSelect(toArray("some text"));
        try {
            subject.initialiseFit();
        } finally {
            verify(t, never()).setEnv(anyString());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException2() throws Exception {
        subject = new TestableDatabaseValueSelect(toArray("DEV"));
        try {
            subject.initialiseFit();
        } finally {
            verify(t, never()).setEnv(anyString());
        }
    }

    @Test
    public void testValue() throws Exception {
        Object returnValue = new Object();
        subject.query = "select";
        when(t.queryForObject(subject.query)).thenReturn(returnValue);
        assertEquals("The expected object was not returned", returnValue, subject.value());

    }

    @Test
    public void testStrValue() throws Exception {
        String returnValue = "return value";
        subject.query = "select";
        when(t.queryForObject(subject.query)).thenReturn(returnValue);
        assertEquals("The expected object was not returned", returnValue, subject.value());
    }

    @Test
    public void testLongValue() throws Exception {
        Long returnValue = 20L;
        subject.query = "select";
        when(t.queryForObject(subject.query)).thenReturn(returnValue);
        assertEquals("The expected object was not returned", (Long) 20L, subject.longValue());
    }

    @Test
    public void testDateValue() throws Exception {
        Date returnValue = new Date();
        subject.query = "select";
        when(t.queryForObject(subject.query)).thenReturn(returnValue);
        assertEquals("The expected object was not returned", returnValue, subject.dateValue());
    }

    String[] toArray(String... array) {
        return array;
    }

    public class TestableDatabaseValueSelect extends DatabaseValueSelect {
        public TestableDatabaseValueSelect(String env, String sql) {
            super(env, sql);
        }
        public TestableDatabaseValueSelect(String[] fitArgs) {
            args = fitArgs;
        }

        @Override
        void configureParameters(String env, String queryString) {
            //we inject the mock transport here, because there is nowhere else to do it in the constructor call chain
            transport = t;
            super.configureParameters(env, queryString);
        }
    }
}