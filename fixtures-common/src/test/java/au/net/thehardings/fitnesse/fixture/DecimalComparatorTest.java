package au.net.thehardings.fitnesse.fixture;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DecimalComparatorTest {
    public static final double VALUE = 10.0;
    DecimalComparator subject;

    @Before
    public void setUp() {
        subject = new DecimalComparator();
    }

    @Test
    public void testCompare() throws Exception {
        subject.setField(VALUE);
        assertEquals("should just have returned the double value", VALUE, 0.0, subject.compare());
    }
}