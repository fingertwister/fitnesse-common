package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.transport.FileTransport;
import freemarker.template.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class FileTemplateEntryTest {
    FileTemplateEntry subject;
    public static final String MESSAGE = "A simple message to be sent to file";
    public static final String TEMPLATE_FILE = "template file";
    public static final String FILE_NAME = "filename";
    public static final String OVERWRITE = "true";

    @Mock
    FileTransport t;
    @Mock
    Configuration c;

    @Test
    public void testFileTemplateEntry() throws Exception {
        subject = new TestableFileTemplateEntry(TEMPLATE_FILE, FILE_NAME, true);
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setFileName(FILE_NAME);
        verify(t).setOverwrite(true);
    }

    @Test
    public void testFileTemplateEntryWithDefault() throws Exception {
        subject = new TestableFileTemplateEntry(TEMPLATE_FILE, FILE_NAME);
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setFileName(FILE_NAME);
        verify(t).setOverwrite(false);
    }

    @Test
    public void testInitialiseFit() throws Exception {
        subject = new TestableFileTemplateEntry(toArray(TEMPLATE_FILE, FILE_NAME, OVERWRITE));
        subject.initialiseFit();
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setFileName(FILE_NAME);
        verify(t).setOverwrite(true);
    }

    @Test
    public void testInitialiseFitWithDefault() throws Exception {
        subject = new TestableFileTemplateEntry(toArray(TEMPLATE_FILE, FILE_NAME));
        subject.initialiseFit();
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setFileName(FILE_NAME);
        verify(t).setOverwrite(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException1() throws Exception {
        subject = new TestableFileTemplateEntry(new String[]{});
        subject.initialiseFit();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException2() throws Exception {
        subject = new TestableFileTemplateEntry(toArray(TEMPLATE_FILE));
        subject.initialiseFit();
    }

    @Test
    public void testSend() throws Exception {
        subject = new TestableFileTemplateEntry();
        subject.transport = t;
        subject.send(MESSAGE);
        verify(t).sendMessage(MESSAGE);
    }

    String[] toArray(String... array) {
        return array;
    }

    public class TestableFileTemplateEntry extends FileTemplateEntry {
        public TestableFileTemplateEntry() {
        }

        public TestableFileTemplateEntry(String templateFile, String fileName) {
            super(templateFile, fileName);
        }

        public TestableFileTemplateEntry(String templateFile, String fileName, boolean overwrite) {
            super(templateFile, fileName, overwrite);
        }

        public TestableFileTemplateEntry(String[] fitArgs) {
            args = fitArgs;
        }

        @Override
        void configureFreemarker() {
            //we inject the mock configuration here, because there is nowhere else to do it in the constructor call chain
            cfg = c;
        }

        @Override
        void configureParameters(String fileName, boolean overwrite) {
            //we inject the mock transport here, because there is nowhere else to do it in the constructor call chain
            transport = t;
            super.configureParameters(fileName, overwrite);
        }
    }
}