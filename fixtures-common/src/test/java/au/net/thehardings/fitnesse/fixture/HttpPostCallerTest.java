package au.net.thehardings.fitnesse.fixture;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import au.net.thehardings.fitnesse.transport.HttpTransport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class HttpPostCallerTest {
    HttpPostCaller subject;
    @Mock
    HttpTransport transport;

    @Before
    public void setUp() {
        subject = new HttpPostCaller();
        subject.transport = transport;
    }

    @Test
    public void testCall() throws Exception {
        subject.setTargetURL("http://localhost/test");
        subject.setUrlParameters("query=coffee,types=good & bad");
        when(transport.executePost("http://localhost/test", "query=coffee&types=good+%26+bad")).thenReturn("Africa and International Roast");
        assertEquals("Response not correct", "Africa and International Roast", subject.call());
    }
}