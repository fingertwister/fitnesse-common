package au.net.thehardings.fitnesse.fixture;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class IntegerComparatorTest {
    public static final int VALUE = 10;
    IntegerComparator subject;

    @Before
    public void setUp() {
        subject = new IntegerComparator();
    }

    @Test
    public void testCompare() throws Exception {
        subject.setField(VALUE);
        assertEquals("should just have returned the double value", VALUE, 0, subject.compare());
    }
}