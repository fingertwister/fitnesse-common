package au.net.thehardings.fitnesse.fixture;

import au.net.thehardings.fitnesse.transport.JmsTransport;
import freemarker.template.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.anyString;

@RunWith(MockitoJUnitRunner.class)
public class JmsTemplateEntryTest {
    JmsTemplateEntry subject;
    public static final String MESSAGE = "A simple message to be sent to jms";
    public static final String TEMPLATE_FILE = "template file";
    public static final String ENVIRONMENT = "DEV";
    public static final String QUEUE = "MY.QUEUE";
    public static final String MSG_TYPE = "TEXT";
    public static final String CHARSET = "iso8859";

    @Mock
    JmsTransport t;
    @Mock
    Configuration c;

    @Test
    public void testJmsTemplateEntry() throws Exception {
        subject = new TestableJmsTemplateEntry(TEMPLATE_FILE, ENVIRONMENT, QUEUE, MSG_TYPE, CHARSET);
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setEnv(ENVIRONMENT);
        verify(t).setQueue(QUEUE);
        assertEquals("JMS Message Type not set properly", JmsTemplateEntry.JmsMessageType.valueOf(MSG_TYPE), subject.messageType);
        assertEquals("Charset name not set properly", CHARSET, subject.charsetName);
    }

    @Test
    public void testJmsTemplateEntryWithDefaultCharset() throws Exception {
        subject = new TestableJmsTemplateEntry(TEMPLATE_FILE, ENVIRONMENT, QUEUE, MSG_TYPE);
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setEnv(ENVIRONMENT);
        verify(t).setQueue(QUEUE);
        assertEquals("JMS Message Type not set properly", JmsTemplateEntry.JmsMessageType.valueOf(MSG_TYPE), subject.messageType);
        assertEquals("Charset name not set to default", JmsTemplateEntry.DEFAULT_CHARSET, subject.charsetName);
    }

    @Test
    public void testJmsTemplateEntryWithDefaultMsgTypeAndCharset() throws Exception {
        subject = new TestableJmsTemplateEntry(TEMPLATE_FILE, ENVIRONMENT, QUEUE);
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setEnv(ENVIRONMENT);
        verify(t).setQueue(QUEUE);
        assertEquals("JMS Message Type not set to default", JmsTemplateEntry.JmsMessageType.valueOf(JmsTemplateEntry.DEFAULT_MSG_TYPE), subject.messageType);
        assertEquals("Charset name not set to default", JmsTemplateEntry.DEFAULT_CHARSET, subject.charsetName);
    }

    @Test
    public void testInitialiseFit() throws Exception {
        subject = new TestableJmsTemplateEntry(toArray(TEMPLATE_FILE, ENVIRONMENT, QUEUE, MSG_TYPE, CHARSET));
        subject.initialiseFit();
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setEnv(ENVIRONMENT);
        verify(t).setQueue(QUEUE);
        assertEquals("JMS Message Type not set properly", JmsTemplateEntry.JmsMessageType.valueOf(MSG_TYPE), subject.messageType);
        assertEquals("Charset name not set properly", CHARSET, subject.charsetName);
    }

    @Test
    public void testInitialiseFitWithDefaultCharset() throws Exception {
        subject = new TestableJmsTemplateEntry(toArray(TEMPLATE_FILE, ENVIRONMENT, QUEUE, MSG_TYPE));
        subject.initialiseFit();
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setEnv(ENVIRONMENT);
        verify(t).setQueue(QUEUE);
        assertEquals("JMS Message Type not set properly", JmsTemplateEntry.JmsMessageType.valueOf(MSG_TYPE), subject.messageType);
        assertEquals("Charset name not set to default", JmsTemplateEntry.DEFAULT_CHARSET, subject.charsetName);
    }

    @Test
    public void testInitialiseFitWithDefaultMsgTypeAndCharset() throws Exception {
        subject = new TestableJmsTemplateEntry(toArray(TEMPLATE_FILE, ENVIRONMENT, QUEUE));
        subject.initialiseFit();
        verify(c).getTemplate(TEMPLATE_FILE);
        verify(t).setEnv(ENVIRONMENT);
        verify(t).setQueue(QUEUE);
        assertEquals("JMS Message Type not set to default", JmsTemplateEntry.JmsMessageType.valueOf(JmsTemplateEntry.DEFAULT_MSG_TYPE), subject.messageType);
        assertEquals("Charset name not set to default", JmsTemplateEntry.DEFAULT_CHARSET, subject.charsetName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException1() throws Exception {
        subject = new TestableJmsTemplateEntry(new String[]{});
        subject.initialiseFit();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException2() throws Exception {
        subject = new TestableJmsTemplateEntry(toArray(TEMPLATE_FILE));
        subject.initialiseFit();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException3() throws Exception {
        subject = new TestableJmsTemplateEntry(toArray(TEMPLATE_FILE, ENVIRONMENT));
        subject.initialiseFit();
    }

    @Test
    public void testSendBytes() throws Exception {
        subject = new TestableJmsTemplateEntry();
        subject.transport = t;
        subject.messageType = JmsTemplateEntry.JmsMessageType.BYTES;
        subject.charsetName = CHARSET;
        subject.send(MESSAGE);
        verify(t).sendBytesMessage(MESSAGE, CHARSET);
    }

    @Test
    public void testSendText() throws Exception {
        subject = new TestableJmsTemplateEntry();
        subject.transport = t;
        subject.messageType = JmsTemplateEntry.JmsMessageType.TEXT;
        subject.send(MESSAGE);
        verify(t).sendMessage(MESSAGE);
    }

    String[] toArray(String... array) {
        return array;
    }

    public class TestableJmsTemplateEntry extends JmsTemplateEntry {
        public TestableJmsTemplateEntry() {
        }

        public TestableJmsTemplateEntry(String templateFile, String env, String queue) {
            super(templateFile, env, queue);
        }

        public TestableJmsTemplateEntry(String templateFile, String env, String queue, String msgType) {
            super(templateFile, env, queue, msgType);
        }

        public TestableJmsTemplateEntry(String templateFile, String env, String queue, String msgType, String charset) {
            super(templateFile, env, queue, msgType, charset);
        }

        public TestableJmsTemplateEntry(String[] fitArgs) {
            args = fitArgs;
        }

        @Override
        void configureFreemarker() {
            //we inject the mock configuration here, because there is nowhere else to do it in the constructor call chain
            cfg = c;
        }

        @Override
        void configureParameters(String env, String queue, String msgType, String charset) {
            transport = t;
            super.configureParameters(env, queue, msgType, charset);
        }
    }
}