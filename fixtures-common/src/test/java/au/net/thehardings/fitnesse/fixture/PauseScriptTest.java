package au.net.thehardings.fitnesse.fixture;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PauseScriptTest {
    public static final Long PAUSE = 1000L;
    PauseScript subject;

    @Before
    public void setUp() {
        subject = new PauseScript();
    }

    @Test
    public void testPauseScript() throws Exception {
        subject = new PauseScript(PAUSE);
        assertEquals("pause time not set correctly for SLiM", PAUSE, 0L, subject.millis);
    }

    @Test
    public void testInitialiseFit() throws Exception {
        subject = new TestablePauseScript(toArray(PAUSE.toString()));
        subject.initialiseFit();
        assertEquals("pause time not set correctly for FIT", PAUSE, 0L, subject.millis);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFit1() throws Exception {
        subject = new TestablePauseScript(null);
        subject.initialiseFit();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFit2() throws Exception {
        subject.initialiseFit();
    }

    @Test
    public void testTable() throws Exception {
        subject.millis = PAUSE;
        long startTime = System.currentTimeMillis();
        subject.table(null);
        long pausePeriod = System.currentTimeMillis() - startTime;
        assertTrue("Thread should have slept happily", pausePeriod >= PAUSE);
    }

    @Test
    public void testTableWithInterrupt() throws Exception {
        subject.millis = PAUSE;
        Thread interruptor = new Interruptor(Thread.currentThread(), 100L);
        interruptor.start();
        long startTime = System.currentTimeMillis();
        subject.table(null);
        long pausePeriod = System.currentTimeMillis() - startTime;
        assertTrue("Thread should have been interrupted with no exception", pausePeriod < PAUSE);
    }

    String[] toArray(String... array) {
        return array;
    }

    public class TestablePauseScript extends PauseScript {
        public TestablePauseScript(String[] fitArgs) {
            args = fitArgs;
        }
    }

    public class Interruptor extends Thread {
        Thread toInterrupt;
        long millis;

        public Interruptor(Thread toInterrupt, long millis) {
            this.toInterrupt = toInterrupt;
            this.millis = millis;
        }

        public void run() {
            //wait the set time
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                //ignore
            }
            //then interrupt the sleeping thread
            toInterrupt.interrupt();
        }
    }
}