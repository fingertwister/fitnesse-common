package au.net.thehardings.fitnesse.fixture;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class TemplateEntryFixtureTest {
    public static final String TEMPLATE_FILE = "template file";
    public static final String MESSAGE = "message";
    TestableTemplateEntryFixture subject;

    @Mock
    Configuration c;

    @Mock
    Template t;

    @Before
    public void setUp() {
        subject = new TestableTemplateEntryFixture();
    }

    @Test
    public void testTemplateEntryFixture() throws Exception {
        assertSame("configuration should be set to the mock object", c, subject.cfg);
    }

    @Test
    public void testTemplateEntryFixtureWithTemplate() throws Exception {
        subject = new TestableTemplateEntryFixture(TEMPLATE_FILE);
        verify(c).getTemplate(TEMPLATE_FILE);
    }

    @Test
    public void testInitialiseFit() throws Exception {
        subject = new TestableTemplateEntryFixture(toArray(TEMPLATE_FILE));
        subject.initialiseFit();
        verify(c).getTemplate(TEMPLATE_FILE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialiseFitException() throws Exception {
        subject = new TestableTemplateEntryFixture(new String[]{});
        try {
            subject.initialiseFit();
        } finally {
            verify(c, never()).getTemplate(TEMPLATE_FILE);
        }
    }

    @Test(expected = RuntimeException.class)
    public void testConfigureParameters() throws Exception {
        when(c.getTemplate(TEMPLATE_FILE)).thenThrow(new IOException());
        subject.configureParameters(TEMPLATE_FILE);
    }

    @Test
    public void testConfigureFreemarker() throws Exception {
        subject.testConfigureFreemarker();
        verify(c).setDirectoryForTemplateLoading(TemplateEntryFixture.PATH_TO_FITNESSE_FILES);
        verify(c).setObjectWrapper(any(DefaultObjectWrapper.class));
        verify(c).setDefaultEncoding("UTF-8");
        verify(c).setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        verify(c).setIncompatibleImprovements(any(Version.class));
        assertEquals("Some default formats should be set", 2, subject.formats.size());
    }

    @Test(expected = RuntimeException.class)
    public void testConfigureFreemarkerException() throws Exception {
        doThrow(new IOException()).when(c).setDirectoryForTemplateLoading(TemplateEntryFixture.PATH_TO_FITNESSE_FILES);
        subject.testConfigureFreemarker();
    }

    @Test
    public void testBeginTable() throws Exception {
        subject.put("key", "value");
        assertEquals("map should be modified", "value", subject.map.get("key"));
        subject.beginTable();
        assertEquals("map should be reset", null, subject.map.get("key"));
    }

    @Test
    public void testExecute() throws Exception {
        subject.template = t;
        doAnswer(new Answer<StringWriter>() {
            public StringWriter answer(InvocationOnMock invocation) throws Throwable {
                StringWriter stringWriter = (StringWriter) invocation.getArguments()[1];
                stringWriter.write(MESSAGE);
                return null;
            }
        }).when(this.t).process(anyObject(), any(StringWriter.class));
        subject.execute();
        assertEquals("Content was not generated", MESSAGE, subject.content);
    }

    @Test
    public void testReset() throws Exception {
        subject.put("key", "value");
        assertEquals("map should be modified", "value", subject.map.get("key"));
        subject.reset();
        assertEquals("map should be reset", null, subject.map.get("key"));
    }

    @Test
    public void testPut1() throws Exception {
        //populates the formats map
        subject.testConfigureFreemarker();
        subject.put("key:Date", "2013-08-31");
        Calendar cal = Calendar.getInstance();
        cal.setTime((Date) subject.map.get("key"));
        assertEquals("Date was not parsed correctly - 1", 2013, cal.get(Calendar.YEAR));
        assertEquals("Date was not parsed correctly - 1", 7, cal.get(Calendar.MONTH));
        assertEquals("Date was not parsed correctly - 1", 31, cal.get(Calendar.DATE));
    }

    @Test
    public void testPut2() throws Exception {
        //populates the formats map
        subject.testConfigureFreemarker();
        subject.put("key:Calendar", "2013-08-31");
        assertEquals("Field was not set correctly", "2013-08-31", subject.map.get("key"));
    }

    @Test
    public void testPut3() throws Exception {
        //populates the formats map
        subject.testConfigureFreemarker();
        subject.put("key:Date", "bad data");
        assertEquals("Field was not set correctly", "bad data", subject.map.get("key"));
    }

    @Test(expected = RuntimeException.class)
    public void testCreateContent() throws Exception {
        subject.template = t;
        doThrow(new Exception()).when(t).process(any(Map.class), any(StringWriter.class));
    }

    String[] toArray(String... array) {
        return array;
    }

    public class TestableTemplateEntryFixture extends TemplateEntryFixture {
        String content;

        public TestableTemplateEntryFixture() {
        }

        public TestableTemplateEntryFixture(String templateFile) {
            super(templateFile);
        }

        public TestableTemplateEntryFixture(String[] fitArgs) {
            args = fitArgs;
        }

        @Override
        void configureFreemarker() {
            cfg = c;
        }

        void testConfigureFreemarker() {
            super.configureFreemarker();
        }

        @Override
        protected void send(String c) {
            content = c;
        }
    }
}