package au.net.thehardings.fitnesse.fixture.fit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import au.net.thehardings.fitnesse.env.DatabaseEnvironment;
import fit.Binding;
import fit.Fixture;
import fit.MapTypeAdapter;
import fit.Parse;
import fit.TypeAdapter;
import fit.exception.FitFailureException;
import fit.exception.FitParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: m034289
 * Date: 30/08/2013
 * Time: 4:03:07 PM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(MockitoJUnitRunner.class)
public class DatabaseQueryTest {
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseQueryTest.class);
    DatabaseQuery subject;
    @Mock
    JdbcTemplate t;

    @Before
    public void setUp() {
        subject = new DatabaseQuery();
    }


    @Test
    public void testDoTable() throws Exception {
        subject = new TestableDatabaseQuery("DEV", "some html text $[middleLine] multiple lines.");
        Fixture.setSymbol("middleLine", "that spans");
        subject.doTable(getParse());
        Fixture.ClearSymbols();
        assertEquals("environment not set correctly", DatabaseEnvironment.factory("DEV"), subject.env);
        assertEquals("environment not set correctly", "some html text that spans multiple lines.", subject.query);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitialise() throws Exception {
        subject = new TestableDatabaseQuery("DEV");
        subject.initialise();
    }

    @Test
    public void testQuery() throws Exception {
        subject.env = DatabaseEnvironment.factory("DEV");
        subject.query = "sql";
        subject.template = t;
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        Map<String, Object> row1 = new HashMap<String, Object>();
        row1.put("id", "1");
        row1.put("name", "Mickey Mouse");
        result.add(row1);
        Map<String, Object> row2 = new HashMap<String, Object>();
        row2.put("id", "2");
        row2.put("name", "Donald Duck");
        result.add(row2);
        when(t.queryForList("sql")).thenReturn(result);
        Object[] returned = subject.query();
        assertEquals(2, returned.length);
        assertEquals("1", ((Map) returned[0]).get("id"));
        assertEquals("Mickey Mouse", ((Map) returned[0]).get("name"));
        assertEquals("2", ((Map) returned[1]).get("id"));
        assertEquals("Donald Duck", ((Map) returned[1]).get("name"));
    }

    @Test
    public void testGetTargetClass() throws Exception {
        assertEquals("The target should be the Map class", Map.class, subject.getTargetClass());
    }

    @Test
    public void testCreateBinding() throws Exception {
        Parse table = getParse();
        table.addToBody("some text");
        Binding binding = subject.createBinding(0, table);
        assertEquals("The binding should have a MapTypeAdapter instance", MapTypeAdapter.class, binding.adapter.getClass());
    }

    @Test
    public void testCheckNormalSuccess() throws Exception {
        Parse heads = getParse();
        TypeAdapter a = subject.createBinding(0,heads.parts.more.parts).adapter;
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("headingA", "valueA1");
        a.target = map;
        subject.check(heads.parts.more.more.parts, a);
        assertEquals("The 'foo' symbol was not replaced properly", "valueA1", heads.parts.more.more.parts.body);
    }

    @Test
    public void testCheckReplaceSuccess() throws Exception {
        Fixture.setSymbol("foo", "bar");
        Parse heads = getParse2();
        TypeAdapter a = subject.createBinding(0,heads.parts.more.parts).adapter;
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("headingA", "bar");
        a.target = map;
        subject.check(heads.parts.more.more.parts, a);
        assertEquals("The 'foo' symbol was not replaced properly", "foo <span class=\"fit_grey\"> = bar</span>", heads.parts.more.more.parts.body);
    }

    Parse getParse() throws FitParseException {
        StringBuilder builder = new StringBuilder();
        builder.append("<table>");
        builder.append("<tr><td>fixture</td><td>param1</td><td>param2</td></tr>");
        builder.append("<tr><td>headingA</td><td>headingB</td><td>headingC</td></tr>");
        builder.append("<tr><td>valueA1</td><td>valueB1</td><td>valueC1</td></tr>");
        builder.append("<tr><td>valueA2</td><td>valueB2</td><td>valueC2</td></tr>");
        builder.append("</table>");
        return new Parse(builder.toString());
    }

    Parse getParse2() throws FitParseException {
        StringBuilder builder = new StringBuilder();
        builder.append("<table>");
        builder.append("<tr><td>fixture</td><td>param1</td><td>param2</td></tr>");
        builder.append("<tr><td>headingA=</td></tr>");
        builder.append("<tr><td>foo</td></tr>");
        builder.append("</table>");
        return new Parse(builder.toString());
    }

    public class TestableDatabaseQuery extends DatabaseQuery {
        public TestableDatabaseQuery(String... fitArgs) {
            args = fitArgs;
        }
    }
}