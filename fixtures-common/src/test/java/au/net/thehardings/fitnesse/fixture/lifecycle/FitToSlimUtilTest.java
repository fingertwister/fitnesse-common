package au.net.thehardings.fitnesse.fixture.lifecycle;

import fit.Parse;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class FitToSlimUtilTest {
    FitToSlimUtil subject;

    @Before
    public void setUp() {
        subject = new FitToSlimUtil();
    }

    @Test
    public void testParseFitTable() throws Exception {
        StringBuilder builder = new StringBuilder();
        builder.append("<table>");
        builder.append("<tr><td>fixture</td><td>param1</td><td>param2</td></tr>");
        builder.append("<tr><td>headingA</td><td>headingB</td><td>headingC</td></tr>");
        builder.append("<tr><td>valueA1</td><td>valueB1</td><td>valueC1</td></tr>");
        builder.append("<tr><td>valueA2</td><td>valueB2</td><td>valueC2</td></tr>");
        builder.append("</table>");
        Parse table = new Parse(builder.toString());
        List<List<String>> parsedTable = subject.parseFitTable(table);
        assertEquals(3, parsedTable.size());
        assertEquals(3, parsedTable.get(0).size());
        assertEquals("headingA", parsedTable.get(0).get(0));
        assertEquals("headingB", parsedTable.get(0).get(1));
        assertEquals("headingC", parsedTable.get(0).get(2));
        assertEquals(3, parsedTable.get(1).size());
        assertEquals("valueA1", parsedTable.get(1).get(0));
        assertEquals("valueB1", parsedTable.get(1).get(1));
        assertEquals("valueC1", parsedTable.get(1).get(2));
        assertEquals(3, parsedTable.get(2).size());
        assertEquals("valueA2", parsedTable.get(2).get(0));
        assertEquals("valueB2", parsedTable.get(2).get(1));
        assertEquals("valueC2", parsedTable.get(2).get(2));
    }

    @Test
    public void testGetArg() throws Exception {
        String[] args = toArray("param1", "param2");
        assertEquals("param1", subject.getArg(args, 1));
        assertEquals("param2", subject.getArg(args, 2));
        assertEquals(null, subject.getArg(args, 3));
        assertEquals(null, subject.getArg(null, 1));
        assertEquals("default", subject.getArg(args, 3, "default"));
        assertEquals("default", subject.getArg(null, 1, "default"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMandatoryArg() throws Exception {
        String[] args = toArray("param1", "param2");
        assertEquals("param1", subject.getMandatoryArg("parameter 1", args, 1));
        assertEquals("param2", subject.getMandatoryArg("parameter 2", args, 2));
        subject.getMandatoryArg("parameter 2", args, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMandatoryArgException() throws Exception {
        subject.getMandatoryArg("parameter 1", null, 1);
    }

    String[] toArray(String... array) {
        return array;
    }
}