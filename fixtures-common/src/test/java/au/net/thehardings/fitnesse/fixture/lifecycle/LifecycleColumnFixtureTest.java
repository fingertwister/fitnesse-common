package au.net.thehardings.fitnesse.fixture.lifecycle;

import fit.Parse;
import fit.exception.FitParseException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class LifecycleColumnFixtureTest {
    TestableLifecycleColumnFixture subject;

    @Before
    public void setUp() {
        subject = new TestableLifecycleColumnFixture();
    }

    @Test
    public void testDoTable() throws Exception {
        subject.doTable(getTable());
        assertTrue(subject.initialiseFitCalled);
        assertTrue(subject.tableCalled);
    }

    @Test
    public void testDoRows() throws Exception {
        subject.doRows(getTable());
        assertTrue(subject.beginTableCalled);
        assertTrue(subject.endTableCalled);
    }

    @Test
    public void testCoverage() throws Exception {
        //coverage only
        subject.execute();
    }

    @Test
    public void testReset() throws Exception {
        //coverage only
        subject.reset();
    }

    Parse getTable() throws FitParseException {
        StringBuilder builder = new StringBuilder();
        builder.append("<table>");
        builder.append("<tr><td>fixture</td><td>param1</td><td>param2</td></tr>");
        builder.append("<tr><td>headingA</td><td>headingB</td><td>headingC</td></tr>");
        builder.append("<tr><td>valueA1</td><td>valueB1</td><td>valueC1</td></tr>");
        builder.append("<tr><td>valueA2</td><td>valueB2</td><td>valueC2</td></tr>");
        builder.append("</table>");
        return new Parse(builder.toString());
    }

    public class TestableLifecycleColumnFixture extends LifecycleColumnFixture {
        boolean initialiseFitCalled;
        boolean tableCalled;
        boolean beginTableCalled;
        boolean endTableCalled;

        @Override
        protected void initialiseFit() {
            super.initialiseFit();
            initialiseFitCalled = true;
        }

        @Override
        public void table(List<List<String>> table) {
            super.table(table);
            tableCalled = true;
        }

        @Override
        public void beginTable() {
            super.beginTable();
            beginTableCalled = true;
        }

        @Override
        public void endTable() {
            super.endTable();
            endTableCalled = true;
        }
    }
}