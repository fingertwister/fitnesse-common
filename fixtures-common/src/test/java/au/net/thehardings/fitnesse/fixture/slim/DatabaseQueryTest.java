package au.net.thehardings.fitnesse.fixture.slim;

import au.net.thehardings.fitnesse.env.DatabaseEnvironment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseQueryTest {
    DatabaseQuery subject;
    @Mock
    JdbcTemplate t;

    @Before
    public void setUp() {
        subject = new DatabaseQuery("DEV", "some html text<br/> that contains<BR/> multiple lines");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testDatabaseQuery() throws Exception {
        assertEquals("Environment not set correctly", DatabaseEnvironment.factory("DEV"), subject.env);
        assertEquals("Environment not set correctly", "some html text that contains multiple lines", subject.query);
    }

    @Test
    public void testQuery() throws Exception {
        subject.template = t;
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        Map<String, Object> row1 = new HashMap<String, Object>();
        row1.put("id", "1");
        row1.put("name", "Mickey Mouse");
        result.add(row1);
        Map<String, Object> row2 = new HashMap<String, Object>();
        row2.put("id", "2");
        row2.put("name", "Donald Duck");
        result.add(row2);
        when(t.queryForList("some html text that contains multiple lines")).thenReturn(result);
        List<List<List<Object>>> returned = subject.query();
        assertNotNull(returned);
        assertEquals("id", returned.get(0).get(0).get(0));
        assertEquals("1", returned.get(0).get(0).get(1));
        assertEquals("name", returned.get(0).get(1).get(0));
        assertEquals("Mickey Mouse", returned.get(0).get(1).get(1));
        assertEquals("id", returned.get(1).get(0).get(0));
        assertEquals("2", returned.get(1).get(0).get(1));
        assertEquals("name", returned.get(1).get(1).get(0));
        assertEquals("Donald Duck", returned.get(1).get(1).get(1));
    }
}