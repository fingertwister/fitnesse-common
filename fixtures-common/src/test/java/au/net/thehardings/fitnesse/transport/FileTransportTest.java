package au.net.thehardings.fitnesse.transport;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;

import static org.junit.Assert.*;

public class FileTransportTest {
    private static final Logger LOG = LoggerFactory.getLogger(FileTransportTest.class);
    public static final String FILE_SUFFIX = "testFile";
    public static final String STANDARD_FILE = FILE_SUFFIX + ".csv";
    public static final String NUMBERED_FILE = FILE_SUFFIX + "-{0}.csv";
    public static final String MESSAGE = "This is a test message";
    FileTransport subject;


    @Before
    public void setUp() {
        subject = new FileTransport();
        clearOldFiles();
    }

    @After
    public void tearDown() {
        clearOldFiles();
    }

    @Test
    public void testSetFileName() throws Exception {
        subject.setOverwrite(true);
        subject.setFileName(STANDARD_FILE);
        assertTrue("Not having a message format should result in overwrite being true - scenario 1", subject.overwrite);
    }

    @Test
    public void testSetFileName1() throws Exception {
        subject.setOverwrite(false);
        subject.setFileName(STANDARD_FILE);
        assertTrue("Not having a message format should result in overwrite being true - scenario 2", subject.overwrite);
    }

    @Test
    public void testSetFileName2() throws Exception {
        subject.setOverwrite(false);
        subject.setFileName(NUMBERED_FILE);
        assertFalse("Having a message format should honour a false overwrite setting", subject.overwrite);
    }

    @Test
    public void testSetFileName3() throws Exception {
        subject.setOverwrite(true);
        subject.setFileName(NUMBERED_FILE);
        assertTrue("Having a message format should honour a true overwrite setting", subject.overwrite);
    }

    @Test
    public void testSendMessage1() throws Exception {
        assertEquals("There should be no test files", 0, getTestFiles().length);
        subject.setFileName(STANDARD_FILE);
        subject.sendMessage(MESSAGE);
        assertEquals("There should be one test file", 1, getTestFiles().length);
        assertEquals("The file name should be correct", STANDARD_FILE, getTestFiles()[0]);
        subject.sendMessage(MESSAGE);
        assertEquals("There should still be one test file", 1, getTestFiles().length);
        assertEquals("The file name should be maintained", STANDARD_FILE, getTestFiles()[0]);
    }

    @Test
    public void testSendMessage2() throws Exception {
        assertEquals("There should be no test files", 0, getTestFiles().length);
        subject.setOverwrite(false);
        subject.setFileName(NUMBERED_FILE);
        subject.sendMessage(MESSAGE);
        assertEquals("There should be one test files", 1, getTestFiles().length);
        assertEquals("The first file name should be correct", FILE_SUFFIX + "-000.csv", getTestFiles()[0]);
        subject.sendMessage(MESSAGE);
        assertEquals("There should be two test files", 2, getTestFiles().length);
        assertEquals("The second file name should be correct", FILE_SUFFIX + "-001.csv", getTestFiles()[1]);
    }

    String[] getTestFiles() {
        File currentDir = new File(".");
        return currentDir.list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(FILE_SUFFIX);
            }
        });
    }

    void clearOldFiles() {
        String[] files = getTestFiles();
        LOG.debug("Deleting {} files...", files.length);
        for (String file : files) {
            File testFile = new File(file);
            if (!testFile.delete()) {
                LOG.warn("Test file '{}' not deleted.", testFile.getName());
            } else {
                LOG.debug("Test file '{}' deleted.", testFile.getName());
            }
        }
    }
}