package au.net.thehardings.fitnesse.transport;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class HttpTransportTest {
    public static final String URL_STRING = "http://localhost:8000/test";
    public static final String RESPONSE = "My Response";

    HttpTransport subject;
    CamelContext context;
    MockEndpoint endpoint;

    @Before
    public void setUp() throws Exception {
        subject = new HttpTransport();
        context = new DefaultCamelContext();
        context.disableJMX();
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("jetty:" + URL_STRING).to("mock:end");
            }
        });
        endpoint = (MockEndpoint) context.getEndpoint("mock:end");
        context.start();
    }

    @After
    public void tearDown() throws Exception {
        context.stop();
    }

    @Test
    public void testExecutePost() throws Exception {
        endpoint.whenAnyExchangeReceived(new Processor() {
            public void process(Exchange exchange) throws Exception {
                Message in = exchange.getIn();

                //assert parameters successfully recieved
                assertEquals("Did not receive parameter one correctly", "value1", in.getHeader("param1"));
                assertEquals("Did not receive parameter two correctly", "value2", in.getHeader("param2"));

                //respond with success
                exchange.getOut().setBody(RESPONSE);
            }
        });
        assertEquals("Did not receive the correct response", RESPONSE, subject.executePost(URL_STRING, "param1=value1&param2=value2").trim());
    }

    @Test(expected = RuntimeException.class)
    public void testExecutePostException() throws Exception {
        subject.executePost("dodgy url string", null);
    }
}