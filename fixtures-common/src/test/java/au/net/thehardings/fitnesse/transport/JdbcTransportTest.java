package au.net.thehardings.fitnesse.transport;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class JdbcTransportTest {
    JdbcTransport subject;

    @Before
    public void setUp() {
        subject = new JdbcTransport();
        subject.setEnv("DEV");
        subject.sendMessage("create table unit_test_user (id int, name varchar(50))");
    }

    @After
    public void tearDown() {
        subject.sendMessage("drop table unit_test_user");
    }

    @Test
    public void testJdbcTransport() throws Exception {
        //create three users
        subject.sendMessage("insert into unit_test_user values (1, 'user1')");
        subject.sendMessage("insert into unit_test_user values (2, 'user2')");
        subject.sendMessage("insert into unit_test_user values (3, 'user3')");
        int id = (Integer) subject.queryForObject("select id from unit_test_user where name = 'user1'");
        int maxId = (Integer) subject.queryForObject("select max(id) from unit_test_user");
        long count = (Long) subject.queryForObject("select count(*) from unit_test_user");
        assertEquals("The record should be inserted based on the insert sendMessage call", 1, id);
        assertEquals("The max(id) in the table should be three", 3, maxId);
        assertEquals("There should be three users", 3, count);
        //delete a user
        subject.sendMessage("delete from unit_test_user where name = 'user2'");
        count = (Long) subject.queryForObject("select count(*) from unit_test_user");
        maxId = (Integer) subject.queryForObject("select max(id) from unit_test_user");
        assertEquals("The max ID should remain unchanged", 3, maxId);
        assertEquals("The user count should be reduced based on the delete sendMessage call", 2, count);
    }

}