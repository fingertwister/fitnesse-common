package au.net.thehardings.fitnesse.transport;

import static org.junit.Assert.*;

import org.apache.activemq.broker.BrokerService;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.jms.JMSException;
import javax.jms.QueueConnection;

public class JmsTransportTest {
    public static final String MESSAGE = "A test message";
    static BrokerService broker;
    TestableJmsTransport subject;

    @BeforeClass
    public static void startBroker() throws Exception {
        broker = new BrokerService();
        broker.setPersistent(false);
        broker.start();
    }

    @Before
    public void setUp() throws Exception {
        subject = new TestableJmsTransport();
        subject.setEnv("DEV");
        subject.setQueue("QUEUE");
        subject.setPauseTime(100);
    }

    @AfterClass
    public static void stopBroker() throws Exception {
        broker.stop();
    }

    @Test
    public void testSendMessage() throws Exception {
        subject.sendMessage(MESSAGE);
        assertEquals("Message was not as expected", MESSAGE, subject.receiveMessage());
    }

    @Test(expected = TransportException.class)
    public void testSendMessageException() throws Exception {
        subject.badQc = true;
        subject.sendMessage(MESSAGE);
    }

    @Test
    public void testSendMessage2() throws Exception {
        subject.sendMessage(MESSAGE.getBytes("UTF-8"));
        assertEquals("Message was not as expected", MESSAGE, subject.receiveMessage());
    }

    @Test(expected = TransportException.class)
    public void testSendMessage2Exception() throws Exception {
        subject.badQc = true;
        subject.sendMessage(MESSAGE.getBytes("UTF-8"));
    }

    @Test
    public void testClearAllMessages() throws Exception {
        subject.sendMessage(MESSAGE);
        subject.sendMessage(MESSAGE);
        subject.sendMessage(MESSAGE);
        assertEquals("Should have cleared 3 messages from queue", 3, subject.clearAllMessages());
        assertEquals("Should have cleared 0 messages from queue", 0, subject.clearAllMessages());
    }

    @Test(expected = TransportException.class)
    public void testClearAllMessagesException() throws Exception {
        subject.badQc = true;
        subject.clearAllMessages();
    }

    @Test
    public void testGetQueueDepth() throws Exception {
        subject.sendMessage(MESSAGE);
        subject.sendMessage(MESSAGE);
        subject.sendMessage(MESSAGE);
        assertEquals("Should have found 3 messages on the queue", 3, subject.getQueueDepth());
        assertEquals("Should have cleared 3 messages from queue", 3, subject.clearAllMessages());
    }

    @Test(expected = TransportException.class)
    public void testGetQueueDepthException() throws Exception {
        subject.badQc = true;
        subject.getQueueDepth();
    }

    class TestableJmsTransport extends JmsTransport {
        boolean badQc = false;

        @Override
        protected QueueConnection getQueueConnection() throws JMSException {
            if (badQc) {
                throw new JMSException("Just for testing");
            }
            return super.getQueueConnection();
        }
    }
}