package au.net.thehardings.fitnesse.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class DefaultEncryptionUtilImplTest {
    TestableEncryptionUtilImpl subject;

    @Before
    public void setUp() {
        subject = new TestableEncryptionUtilImpl();
    }

    @Test
    public void testEncrypt() throws Exception {
        assertEquals("null not supported", "", subject.encrypt("salt", null));
        assertEquals("empty string not supported", "", subject.encrypt("salt", ""));
        assertEquals("null salt not supported", "h98nS2iV9e3SKs5GyCdXuQ==", subject.encrypt(null, "password"));
        assertEquals("empty salt not supported", "h98nS2iV9e3SKs5GyCdXuQ==", subject.encrypt("", "password"));
        assertEquals("encryption did not work", "8/vRcwKaeoio+lCMXbikMQ==", subject.encrypt("salt", "password"));
    }

    @Test
    public void testEncryptWithException() throws Exception {
        subject.makeException = true;
        assertEquals("null not supported", "password", subject.encrypt("salt", "password"));
    }
    @Test
    public void testDecrypt() throws Exception {
        assertEquals("null not supported", "", subject.decrypt("salt", null));
        assertEquals("empty string not supported", "", subject.decrypt("salt", ""));
        assertEquals("null salt not supported", "password", subject.decrypt(null, "h98nS2iV9e3SKs5GyCdXuQ=="));
        assertEquals("empty salt not supported", "password", subject.decrypt("", "h98nS2iV9e3SKs5GyCdXuQ=="));
        assertEquals("encryption did not work", "password", subject.decrypt("salt", "8/vRcwKaeoio+lCMXbikMQ=="));
    }

    @Test
    public void testDecryptWithException() throws Exception {
        subject.makeException = true;
        assertEquals("null not supported", "password", subject.decrypt("salt", "password"));
    }

    @Test
    public void testGetPassphrase() throws Exception {
        assertEquals("no support for null passphrases", "asdl;d09ApD0nmX#Og=82-1[", subject.getPassphrase(null));
        assertEquals("no support for empty passphrases", "asdl;d09ApD0nmX#Og=82-1[", subject.getPassphrase(""));
        assertEquals("encryptionKey was not changed with small salt", "salt;d09ApD0nmX#Og=82-1[", subject.getPassphrase("salt"));
        assertEquals("encryptionKey was not replaced with large salt", "a very long string that is used with testing", subject.getPassphrase("a very long string that is used with testing"));
    }

    public class TestableEncryptionUtilImpl extends DefaultEncryptionUtilImpl {
        public boolean makeException = false;
        @Override
        Cipher getCipher(String encryptionKey, int mode) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException {
            //simple passthrough or override to cause null pointer for exception scenarios
            return makeException ? null : super.getCipher(encryptionKey, mode);
        }
    }
}