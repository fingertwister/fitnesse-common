package au.net.thehardings.fitnesse.util;

import fit.Fixture;
import org.junit.After;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * A JUnit test class for DatabaseQuery
 */
public class StringUtilTest {
    StringUtil subject;

    @Before
    public void setUp() {
        subject = new StringUtil();
    }

    @After
    public void tearDown() {
        Fixture.ClearSymbols();
    }

    @Test
    public void testReplaceSymbols() throws Exception {
        //set up the replaceSymbols test
        String query = "select * from unit_test_user where id = $[id] and name = '$[name]'";

        Fixture.setSymbol("id", 1);
        Fixture.setSymbol("name", "user1");

        //execute the replaceSymbols test
        query = subject.replaceSymbols(query);
        assertEquals("Query string substitution did not work as expected.", "select * from unit_test_user where id = 1 and name = 'user1'", query);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReplaceSymbolsException() throws Exception {
        //set up the replaceSymbols test
        String query = "select * from unit_test_user where id = $[id] and name = '$[name]'";

        //execute the replaceSymbols test
        subject.replaceSymbols(query);
    }

    @Test
    public void testStripHtmlLineBreaks() throws Exception {
        String htmlText = "some html text<br/> that spans<BR/> multiple lines.";

        //execute the replaceSymbols test
        htmlText = subject.stripHtmlLineBreaks(htmlText);
        assertEquals("some html text that spans multiple lines.", htmlText);

    }
}
