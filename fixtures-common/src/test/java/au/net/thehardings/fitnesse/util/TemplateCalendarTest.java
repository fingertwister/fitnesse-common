package au.net.thehardings.fitnesse.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;


public class TemplateCalendarTest {
    TemplateCalendar subject;

    @Before
    public void setUp() {
        subject = new TemplateCalendar();
        subject.set(Calendar.YEAR, 2013);
        subject.set(Calendar.MONTH, 7);
        subject.set(Calendar.DAY_OF_MONTH, 29);
    }

    @Test
    public void testAdd() throws Exception {
        Calendar result = subject.add(5);
        assertEquals("Days did not get added on correctly", 3, result.get(Calendar.DAY_OF_MONTH));
    }
}